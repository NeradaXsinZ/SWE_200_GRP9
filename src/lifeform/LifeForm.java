/*
 * Programmer:Reginald Nettey
 * Professor:Dr. Girard 
 * Course:Design Patterns
 * Description:Creates a LifeForm
 */

package lifeform;

import state.OutOfAmmo;
import environment.Environment;
import weapon.Weapon;
import gameplay.TimeObserver;

public abstract class LifeForm implements TimeObserver
{
    protected int maxLifePoints;

    private String myName;

    protected int currentLifePoints;

    private final int DEFAULT = -1;

    private int x = DEFAULT, y = DEFAULT;

    protected int attackStr;

    public int currentRound;

    public Weapon wep = null;

    Environment world = Environment.getWorldInstance();

    public String currentDirection = Environment.direction[0];

    public int maxSpeed = 0;

    public int wepWanted = 0;

    /**
     * create an instance
     * 
     * @param name the name of the life form
     * @param points the current starting life points of the life form.
     */
    public LifeForm(String name, int points)
    {
        myName = name;
        if (points < 0)
            points = 0;
        currentLifePoints = points;
        maxLifePoints = points;
    }

    /**
     * Overloaded constructor of lifeform
     * @param name name of the lifeform
     * @param points the hitpoints
     * @param attackStr the attack strength of the lifeform
     */
    public LifeForm(String name, int points, int attackStr)
    {
        myName = name;
        if (points < 0)
            points = 0;
        if (attackStr < 0)
            attackStr = 0;
        currentLifePoints = points;
        this.attackStr = attackStr;
    }

    /**
     * Allows you to set back to max health
     * 
     */
    public void setToMaxHealth()
    {
        currentLifePoints = maxLifePoints;
    }

    /**
     * Used to set maxSpeed
     * @param maxSpeed the desired speed
     */
    public void setMaxSpeed(int maxSpeed)
    {
        this.maxSpeed = maxSpeed;
    }

    /**
     * @return the name of the life form.
     */
    public String getName()
    {
        return myName;
    }

    /**
     * @return the amount of current life points the life form has
     */
    public int getCurrentLifePoints()
    {
        if (currentLifePoints < 0)
            currentLifePoints = 0;
        return currentLifePoints;
    }

    /**
     * Take damage from a hit 
     * @param damage
     */
    public void takeHit(int damage)
    {
        if (currentLifePoints >= damage)
        {
            currentLifePoints -= damage;
            
        }
        else
            currentLifePoints = 0;

        if (currentLifePoints <= 0 && getXLoc() != DEFAULT)
        {
            world.removeLifeFormFromEnvironment(getXLoc(), getYLoc());
        }

    }

    /**
     * Turns a lifeform to whichever direction they wish
     * @param str Input a direction 
     */
    public void turn(String str)
    {
        // str = str.toUpperCase();
        for (int i = 0; i < 4; i++)
        {
            if (str.toUpperCase().equals(Environment.direction[i]))
            {
                currentDirection = Environment.direction[i];
            }
        }
    }

    /**
     * Gets the current direction
     * @return current direction
     */
    public String getCurrentDirection()
    {
        return currentDirection;
    }

    /**
     * Set the attack strength
     * @param attackStr the strength of the lifeform
     */
    public void setAttackStrength(int attackStr)
    {
        this.attackStr = attackStr;
    }

    /**
     * @return attack Strength
     */
    public int getAttackStrength()
    {
        return attackStr;
    }

    /**
     * Method to attack another lifeform
     * @param lf the lifeForm being attacked
     */
    public void attack(LifeForm lf)
    {
        int range = 0;
        if (Environment.getWorldInstance() != null)
            range = Environment.getWorldInstance().computeRange(lf, this);
        if ((wep != null) && (wep.getCurrentAmmo() > 0) && currentLifePoints > 0)
        {
            wep.setRange(range);
            lf.takeHit(wep.calculateDamage());
            

        }
        else if (currentLifePoints != 0)
        {
            lf.takeHit(attackStr);
           
        }
        

    }

    /**
     * Store the current time in round 
     * updateTime in alien overrides this
     * @param time the current round
     */
    public void updateTime(int time)
    {
        currentRound = time;
    }

    /**
     * Allows the human to pick up a weapon
     * 
     * @param wep Weapon you wish to wield.
     * @author David Burkett (Disco-Dave)
     */
    public void addWeapon(Weapon wep)
    {
        if (this.wep == null)
            this.wep = wep;
    }

    /**
     * 
     * @return the weapon the lifeForm is carrying
     */
    public Weapon getWeapon()
    {
        return wep;
    }

    /**
     * This will drop the current weapon
     */
    public void dropWeapon()
    {
        // if(world !=null &&wep!=null && world.getCellArr()[getXLoc()][getYLoc()].getNumWeapons()<2)
        //     world.addWeaponToEnvironment(getXLoc(), getYLoc(), wep);
        this.wep = null;
    }

    /**
     * set Location
     * @param x the x location
     * @param y the y location
     */
    public void setLoc(int x, int y)
    {
        this.x = x;
        this.y = y;
    }

    /**
     * Get the x location
     * @return the x location
     */
    public int getXLoc()
    {
        return x;
    }

    /**
     * Get the y location
     * @return the y location
     */
    public int getYLoc()
    {
        return y;
    }

    /**
     * Get the max life points of a life form
     * @return maxLifePoints
     */
    public int getMaxLifePoints()
    {
        return maxLifePoints;
    }

    /**
     * Set the number of max life points
     * @param maxLifePoints
     */
    public void setMaxLifePoints(int maxLifePoints)
    {
        this.maxLifePoints = maxLifePoints;
    }

}