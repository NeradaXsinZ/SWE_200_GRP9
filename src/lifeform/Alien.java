/*
 * Programmer:Reginald Nettey
 * Professor:Dr. Girard 
 * Course:Design Patterns
 * Description:An alien that is a subclass of LifeForm
 */
package lifeform;

import recovery.*;

public class Alien extends LifeForm
{

    private int recoveryRate;

    RecoveryBehavior recoveryBehavior;

    /**
     * Allows creation of an instance of alien
     * 
     * @param name name of the Alien
     * @param points number of Hit points the alien begins with
     * @param recoveryBehavior the recovery behavior of the alien(none, linear, fractional)
     */
    public Alien(String name, int points, RecoveryBehavior recoveryBehavior, int recoveryRate)
    {
        super(name, points);
        this.maxLifePoints = points;
        this.recoveryBehavior = recoveryBehavior;
        super.setAttackStrength(10);
        this.recoveryRate = recoveryRate;
        maxSpeed = 2;
    }

    /**
     * Allows to set value for current life points
     * 
     * @param points the current life points of the alien
     */
    public void setCurrentLifePoints(int points)
    {
        currentLifePoints = points;
    }

    /**
     * Set the recovery rate of the alien(in rounds)
     * @param recoveryRate
     */
    public void setRecoveryRate(int recoveryRate)
    {
        this.recoveryRate = recoveryRate;
    }

    /**
     * Get the recovery rate
     * @return recovery rate of the alien
     */
    public int getRecoveryRate()
    {
        return recoveryRate;
    }

    /**
     * Recover using the specified recovery behavior 
     */
    public void recover()
    {
        setCurrentLifePoints(recoveryBehavior.calculateRecovery(currentLifePoints, maxLifePoints));
    }

    /**
     * overrides the updateTime in lifeform
     * recovers
     * @param time
     */
    @Override
    public void updateTime(int time)
    {
        if (recoveryRate != 0 && time % recoveryRate == 0)
            recover();
        currentRound = time;
    }
}
