/*
 * Programmer:Reginald Nettey
 * Professor:Dr. Girard 
 * Course:Design Patterns
 * Description:A Human that is a subclass of LifeForm
 */
package lifeform;

public class Human extends LifeForm
{
    int armorPoints;

    /**
    * Creates an instance of a human
    * 
    * @param name the name of the human
    * @param points the number of life points the human has
    * @param armorPoints the number of armorPoints the human has
    */
    public Human(String name, int points, int armorPoints)
    {
        super(name, points);
        if (armorPoints < 0)
        {
            this.armorPoints = 0;
        }
        else
            this.armorPoints = armorPoints;
        super.setAttackStrength(5);
        maxSpeed = 3;
    }

    /**
     * Accessor for armorPoints
     * 
     * @return the number of armor points
     */
    public int getArmorPoints()
    {
        if (armorPoints < 0)
        {
            armorPoints = 0;
        }
        return armorPoints;
    }

    /**
     * Mutator for armorPoints
     * 
     * @param armorPoints the number of armorPoints a human has
     */
    public void setArmorPoints(int armorPoints)
    {
        this.armorPoints = armorPoints;
    }

    @Override
    public void takeHit(int damage)
    {
        if (damage - armorPoints <= 0)
            damage = 0;
        else
            damage -= armorPoints;
        currentLifePoints -= damage;
    }
}
