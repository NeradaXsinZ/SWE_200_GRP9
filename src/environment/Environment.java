/*
 * Programmer:Reginald Nettey
 * Professor:Dr. Girard 
 * Course:Design Patterns
 * Description:An environment that can hold cells
 */
package environment;

import weapon.Weapon;
import lifeform.Human;
import lifeform.LifeForm;

public class Environment
{
    /**
     * Declare variables
     */
    private Cell[][] cellArr = new Cell[5][5];

    private Cell cell;

    private static Environment theWorld;

    public static final String direction[] = { "NORTH", "EAST", "SOUTH", "WEST" };

    int maxRow = -1;

    int maxCol = -1;
    
    public int numHumans=0;
    
    public int numAliens=0;

	public int numWeapons=0;

    /**
     * Create an instance of an environment
     * 
     * @param row
     *            the number of rows in the environment
     * @param col
     *            the number of columns in the environment
     */
    private Environment(int row, int col)
    {
        setCellArr(new Cell[row][col]);
        maxRow = row;
        maxCol = col;
    }

    /**
     * A method that will move a lifeform in the direction they are facing and
     * at their maxspeed. It will stop at borders and at obstacles
     * @author David Burkett
     * @param form1
     *            - The lifeform you wish to move
     */
    public void move(LifeForm form1)
    {

        int currRow = form1.getXLoc();
        int currCol = form1.getYLoc();

        try
        {
            if (form1.currentDirection.equals("NORTH"))
            {

                for (int i = 1; i <= form1.maxSpeed; i++)
                {
                    if (currCol - i >= 0)
                    {
                        if ((theWorld.getCellArr()[currRow][currCol - i] == null) || (theWorld.getCellArr()[currRow][currCol - i].getLifeForm() == null))
                        {
                            theWorld.removeLifeFormFromEnvironment(form1.getXLoc(), form1.getYLoc());
                            theWorld.addLifeFormToEnvironment(currRow, currCol - i, form1);
                        }
                        else
                            i = form1.maxSpeed + 1;
                    }
                }

            }

            if (form1.currentDirection.equals("SOUTH"))
            {

                for (int i = 1; i <= form1.maxSpeed; i++)
                {

                    if (currCol + i <= theWorld.getMaxCol())

                    {
                        if ((theWorld.getCellArr()[currRow][currCol + i] == null) || (theWorld.getCellArr()[currRow][currCol + i].getLifeForm() == null))
                        {
                            theWorld.removeLifeFormFromEnvironment(form1.getXLoc(), form1.getYLoc());
                            theWorld.addLifeFormToEnvironment(currRow, currCol + i, form1);
                        }
                        else
                            i = form1.maxSpeed + 1;
                    }
                }

            }

            if (form1.currentDirection.equals("EAST"))
            {

                for (int i = 1; i <= form1.maxSpeed; i++)
                {
                    if (currRow + i <= theWorld.getMaxRow())
                    {
                        if ((theWorld.getCellArr()[currRow + i][currCol] == null) || (theWorld.getCellArr()[currRow + i][currCol].getLifeForm() == null))
                        {
                            theWorld.removeLifeFormFromEnvironment(form1.getXLoc(), form1.getYLoc());
                            theWorld.addLifeFormToEnvironment(currRow + i, currCol, form1);
                        }
                        else
                            i = form1.maxSpeed + 1;
                    }
                }

            }

            if (form1.currentDirection.equals("WEST"))
            {

                for (int i = 1; i <= form1.maxSpeed; i++)
                {
                    if (currRow - i >= 0)
                    {
                        if ((theWorld.getCellArr()[currRow - i][currCol] == null) || (theWorld.getCellArr()[currRow - i][currCol].getLifeForm() == null))
                        {
                            theWorld.removeLifeFormFromEnvironment(form1.getXLoc(), form1.getYLoc());
                            theWorld.addLifeFormToEnvironment(currRow - i, currCol, form1);
                        }
                        else
                            i = form1.maxSpeed + 1;
                    }
                }
            }
        }
        catch (IndexOutOfBoundsException e)
        {
        }
    }

    /**
     * Add a LifeForm to the environment
     * 
     * @param row
     *            the row in which to add the LifeForm
     * @param col
     *            the column in which to add the LifeForm
     * @param life1
     *            the LifeForm which is to be added
     * @return true if add and false otherwise
     */
    public boolean addLifeFormToEnvironment(int row, int col, LifeForm form1)
    {
        if (row > getCellArr().length - 1 || col > getCellArr()[0].length - 1 || row < 0 || col < 0)
        {
            return false;
        }
        else
        {
            if (getCellArr()[row][col] == null)
            {

                cell = new Cell();
                form1.setLoc(row, col);
                cell.addLifeForm(form1);
                getCellArr()[row][col] = cell;
                if(form1 instanceof Human)
                	numHumans++;
                else numAliens++;
                
                return true;
            }
            else if (getCellArr()[row][col].entity == null)
            {
                form1.setLoc(row, col);
                if(form1 instanceof Human)
                	numHumans++;
                else numAliens++;
                return getCellArr()[row][col].addLifeForm(form1);

            }
            else if (getCellArr()[row][col].getNumWeapons() > 0)
            {
                form1.setLoc(row, col);
                if(form1 instanceof Human)
                	numHumans++;
                else numAliens++;
                return getCellArr()[row][col].addLifeForm(form1);
            }
            return false;
        }
    }

    /**
     * Add a weapon to the environment
     * 
     * @param row
     *            the row in which the weapon will be
     * @param col
     *            the column in which the weapon will be
     * @param wep
     *            the weapon to be added
     * @return true if added false otherwise
     */
    public boolean addWeaponToEnvironment(int row, int col, Weapon wep)
    {
        if (row > getCellArr().length - 1 || col > getCellArr()[0].length - 1 || row < 0 || col < 0)
        {
            return false;
        }
        else
        {
            if (getCellArr()[row][col] == null)
            {
                cell = new Cell();
                cell.addWeapon(wep);
                numWeapons++;
                getCellArr()[row][col] = cell;
                return true;
            }
            else
            {
                numWeapons++;
                return getCellArr()[row][col].addWeapon(wep);
            }
        }
    }

    /**
     * Remove an element from a certain index in the 2d array
     * 
     * @param row
     *            the row from witch to remove
     * @param col
     *            the column from which to remove return true if it was removed
     *            false otherwise
     */
    public boolean removeLifeFormFromEnvironment(int row, int col)
    {
        try
        {
            if (getCellArr()[row][col] != null)
            {
            	 if(getCellArr()[row][col].getLifeForm() instanceof Human)
                 	numHumans--;
                 else numAliens--;
            	 
                getCellArr()[row][col].removeLifeForm();
               
                return true;
            }
        }
        catch (IndexOutOfBoundsException e)
        {
            return false;
        }
        return false;
    }

    /**
     * Removes a specific weapon from the a certain index in the 2d array
     * 
     * @param row
     *            the row in which to add the weapon
     * @param col
     *            the column in which to add the weapon
     * @param wep
     *            the weapon to be added
     * @return True if the weapon was removed, false otherwise
     */
    public boolean removeWeaponFromEnvironment(int row, int col, Weapon wep)
    {
        try
        {
            getCellArr()[row][col].removeWeapon(wep);
            return true;
        }
        catch (NullPointerException e)
        {
            return false;
        }
        catch (IndexOutOfBoundsException f)
        {
            return false;
        }
    }

    /**
     * Getter for the instance of the world
     * 
     * @return the environment
     */
    public static Environment getWorldInstance()
    {
        return theWorld;
    }

    /**
     * Makes one instance of environment and no more
     * 
     * @param row
     *            the row size of the environment
     * @param col
     *            the column size of the environment
     */
    public static void makeEnvironment(int row, int col)
    {
        if (theWorld == null)
        {
            theWorld = new Environment(row, col);
        }
    }

    /**
     * Destroys the environment so a new instance may be created
     */
    public void destroyEnvironment()
    {
        theWorld = null;
    }

    /**
     * Calculates the range between two lifeforms
     * 
     * @param form1
     *            the first life form
     * @param form2
     *            the second life form
     * @return the distance between the two life forms
     */
    public int computeRange(LifeForm f1, LifeForm f2)
    {
        int range = 0, deltaX, deltaY;
        ;

        if (f1.getXLoc() != -1 && f2.getXLoc() != -1)
        {
            deltaX = 10 * Math.abs(f1.getXLoc() - f2.getXLoc());
            deltaY = 10 * Math.abs(f1.getYLoc() - f2.getYLoc());
            range = (int) Math.sqrt((int) Math.pow(deltaX, 2) + (int) Math.pow(deltaY, 2));
        }
        // I Have it throw a null pointer here in case the lifeform isnt found
        // in the array
        else
            throw new NullPointerException();
        return range;
    }

    public LifeForm getLifeForm(int row, int column)
    {
        if (cellArr[row][column] != null && cellArr[row][column].getLifeForm() != null)
            return cellArr[row][column].getLifeForm();
        return null;
    }

    public String getLifeFormType(int row, int column)
    {
        if (cellArr[row][column] != null && cellArr[row][column].getLifeForm() != null)
        {
            LifeForm lifeform = cellArr[row][column].getLifeForm();
            if (lifeform instanceof Human)
            {
                return "Human";
            }
            else
            {
                return "Alien";
            }
        }
        return " ";

    }

    public Cell[][] getCellArr()
    {
        return cellArr;
    }

    private void setCellArr(Cell[][] cellArr)
    {
        this.cellArr = cellArr;
    }

    /**
     * 
     * @param x row x
     * @param y col y
     * @param i	spot i 
     * @return return if there is a gun in spot i of cell[x][y]
     */
    public boolean isThereAGun(int x, int y, int i)
    {

        if (cellArr[x][y] != null) //if there is a cell at x,y
        {
            int weapons = cellArr[x][y].getNumWeapons(); //number of weapons in cell

            if (weapons > i) //with 1 weapon i can equal 0 and 2 weapon i can be 0 or 1
            {
                return true; //there is a gun
            }
        }
        return false; //no gun
    }

    /**
     * 
     * @param x row x
     * @param y cell y
     * @return	return direction of the cellArr[x][y]
     */
    public String getDirection(int x, int y)
    {
        if (cellArr[x][y] != null)
        {
            return cellArr[x][y].getDirection();
        }
        return " "; //no direction
    }

    /**
     * 
     * @param x 	row x
     * @param y		row y
     * @return		the type of weapon the lifeform has
     */
    public String getLifeFormWeapon(int x, int y)
    {
        if (cellArr[x][y] != null)
        {
            return cellArr[x][y].getLifeFormWeapon();
        }
        return " "; //no weapon
    }

    /**
     * 
     * @param x row x
     * @param y	row y
     * @param i	spot i 
     * @return	cell[x][y] spoti gun type
     */
    public String getGunType(int x, int y, int i)
    {
        if (cellArr[x][y] != null)
        {
            return cellArr[x][y].getGunType(i);
        }
        return " "; //no gun
    }

    /**
     * gives the maxRows in the environment
     * @return maxRow
     */
    public int getMaxRow()
    {
        return maxRow;
    }

    /**
     * gives the maxCols in the environment
     */
    public int getMaxCol()
    {
        return maxCol;
    }

}
