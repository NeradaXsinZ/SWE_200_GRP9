package environment;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

/**
 * 
 * @author Kyle Jones and Tim Haus
 *GUI for the map
 */
public class SampleGUI extends JFrame implements ActionListener
{

    JButton textButton, imageButton;

    JLabel textLabel, imageLabel;

    public static void main(String[] args)
    {
        SampleGUI map = new SampleGUI();
    }

    public SampleGUI()
    {
        setLayout(new BorderLayout());

        JPanel centerPanel = new JPanel(new GridLayout(2, 4));
        JLabel[][] labelArray = new JLabel[2][3];
        for (int r = 0; r < 2; r++)
        {
            for (int c = 0; c < 3; c++)
            {
                labelArray[r][c] = new JLabel(createImage());
                centerPanel.add(labelArray[r][c]);
            }
        }
        add("Center", centerPanel);

        JLabel legend = new JLabel(legend());
        //add(legend);

        pack();
        setVisible(true);
    }

    public ImageIcon createImage()
    {
        BufferedImage exampleImage = new BufferedImage(600, 450, BufferedImage.TYPE_3BYTE_BGR);
        Graphics drawer = exampleImage.getGraphics();

        drawer.setColor(new Color(0, 0, 255));
        drawer.fillRect(0, 0, 50, 50);

        drawer.setColor(new Color(0, 255, 0));
        drawer.fillOval(20, 20, 10, 10);

        drawer.draw3DRect(5, 5, 10, 20, true);

        return new ImageIcon(exampleImage);
    }

    public ImageIcon legend()
    {
        BufferedImage legend = new BufferedImage(100, 450, BufferedImage.TYPE_3BYTE_BGR);
        Graphics drawer = legend.getGraphics();

        drawer.setColor(new Color(255, 0, 0));
        drawer.fillRect(500, 0, 100, 450);

        return new ImageIcon(legend);
    }

    @Override
    public void actionPerformed(ActionEvent event)
    {
        if (event.getSource() == textButton)
        {
            textButton.setText("Clicked ");
        }

    }

}
