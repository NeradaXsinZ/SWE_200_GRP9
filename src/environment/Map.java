/**
 * @author Tim Haus and Kyle Jones
 * Runs a the environment GUI
 */
package environment;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Container;
import java.awt.Dimension;
import java.awt.Graphics;
import java.awt.GridLayout;
import java.awt.Image;
import java.awt.Rectangle;
import java.awt.image.BufferedImage;

import javax.swing.ImageIcon;
import javax.swing.JButton;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.JPanel;

import ui.command.InvokerObserver;
import lifeform.Human;

public class Map extends JFrame implements InvokerObserver
{
    JPanel p = new JPanel();

    Boolean updated;

    JLabel[][] cells = new JLabel[10][10];

    /**
     * @TODO Tim Haus
     * Main Method for creating the map
     * @param args
     */

    public static void main(Environment environment)
    {
        new Map(environment);
    }

    /**
     * @TODO Tim Haus
     * The method that creates the map
     */
    public Map(Environment environment)
    {

        super("Aliens VS. Humans"); //Name of the title

        int maxRow = environment.maxRow;
        int maxCol = environment.maxCol;

        p.setSize(400, 400);
        p.setLayout(new GridLayout(maxRow, maxCol, 5, 5)); //Setup the map grid
        p.setBackground(Color.WHITE);

        JLabel legend = new JLabel(legend()); //Makes the label for the legend
        JLabel[][] labelArray = cells; //Makes the label array of jlabels
        JLabel[][] labelArra = cells; //Makes the label array of jlabels

        setFocusable(false); //This, as well as the next make window unfocusable
        setFocusableWindowState(false);
        for (int c = 0; c < maxCol; c++) //for loop to draw the grid of jlabels
        {
            for (int r = 0; r < maxRow; r++)
            {
                labelArray[r][c] = new JLabel(createImage(environment, r, c)); //Makes the image part of the cell jlabel
                p.add(labelArra[r][c]);

            }
        }
        add(p); //adds the panel
        add(legend, BorderLayout.EAST); //adds the legend
        setVisible(true); //sets the panel view to visable

        pack(); //Makes the file tight to the frame

    }

    /**
     * TODO Kyle Jones and Tim Haus
     * @param environment		the environment to create the map with
     */
    public void updateMap(Environment environment)
    {
        int maxRow = environment.maxRow; //environments max row
        int maxCol = environment.maxCol; //environments max col

        JLabel[][] labelArray = cells; //use labelArry for the instance of the map
        p.removeAll(); //remove all 

        p.setSize(400, 400);
        p.setLayout(new GridLayout(maxRow, maxCol, 5, 5)); //Setup the map grid
        p.setBackground(Color.WHITE);

        for (int c = 0; c < maxCol; c++) //for loop to draw the grid of jlabels
        {
            for (int r = 0; r < maxRow; r++)
            {

                labelArray[r][c] = new JLabel(createImage(environment, r, c)); //Makes the image part of the cell jlabel
                p.add(labelArray[r][c]); //adds it to the panel
            }
        }
        add(p); //adds the panel

        setVisible(true); //sets the panel view to visable

        pack(); //Makes the file tight to the frame

    }

    /**
     * @TODO Tim Haus
     * @return
     */
    private ImageIcon legend()
    {
        BufferedImage legend = new BufferedImage(200, 420, BufferedImage.TYPE_3BYTE_BGR); //Makes a new buffered image
        Graphics draw = legend.getGraphics(); //Makes a new graphics object

        draw.setColor(new Color(0, 0, 255)); //sets the color to blue
        draw.fillRect(0, 0, 200, 835); //makes the rectangle of the whole legend

        draw.setColor(new Color(0, 255, 0)); //sets the color to green
        draw.fillOval(40, 50, 20, 20); //makes the ovel for the humans                                          
        draw.drawString("-Human", 85, 65); //Makes a string with the human name

        draw.setColor(Color.ORANGE); //sets the color to orange
        draw.fillRoundRect(40, 90, 20, 20, 20, 5); //Makes the round rectangle for aliens
        draw.drawString("-Alien", 85, 105); //makes the string for the human name            

        draw.setColor(Color.MAGENTA); //Sets the color to magenta
        draw.fillOval(40, 130, 20, 20); //Makes the oval for the pistol
        draw.drawString("-Pistol", 85, 145); //makes the string for the pistol

        draw.setColor(Color.YELLOW); //sets the color to yellow
        draw.fillOval(40, 170, 20, 20); //makes the oval for the chain gun
        draw.drawString("-Chain Gun", 85, 185); //makes the string for the chain gun

        draw.setColor(Color.WHITE); //sets the color to white
        draw.drawOval(40, 210, 20, 20); //makes the oval for the plasma cannon
        draw.drawString("-Plasma Cannon", 85, 225); //Makes the string for the plasma cannon

        draw.setColor(Color.WHITE); //sets the color to white
        draw.drawLine(40, 250, 70, 250); //Makes the line for the direction
        draw.drawString("-Direction", 85, 255); //Makes the string for the direction

        return new ImageIcon(legend); //returns that image
    }

    /**
     * @TODO Tim Haus and Kyle Jones
     * makes the image of the cells
     * @return
     */

    public ImageIcon createImage(Environment environment, int x, int y)
    {
        BufferedImage exampleImage = new BufferedImage(100, 100, BufferedImage.TYPE_3BYTE_BGR);
        Graphics drawer = exampleImage.getGraphics();

        drawer.setColor(new Color(0, 0, 0));
        drawer.fillRect(0, 0, 75, 75); //makes the cells

        //if the environment has a human in the cell in position x,y
        if (environment.getLifeFormType(x, y).equals("Human"))
        {
            drawHuman(exampleImage); //draw Human
        }

        //if the environment has an alien at the cell in position (x,y)
        if (environment.getLifeFormType(x, y).equals("Alien"))
        {
            drawAlien(exampleImage); //draw Alien
        }

        if (environment.isThereAGun(x, y, 0)) //if there is a gun at x,y spot 1
        {
            if (environment.getGunType(x, y, 0) == "Chain Gun") //if the gun is a ChainGun in spot 1
            {
                drawChainGun(0, 0, exampleImage); //draw ChainGun at 0,0
            }

            if (environment.getGunType(x, y, 0) == "Plasma Cannon") //if the gun is a Plasma Cannon
            {
                drawPlasmaCannon(0, 0, exampleImage); //draw Plasma Cannon at 0,0
            }

            if (environment.getGunType(x, y, 0) == "Pistol") //if the gun is a Pistol
            {
                drawPistol(0, 0, exampleImage); //draw Pistol at 0,0
            }
        }

        //Second set of weapons
        if (environment.isThereAGun(x, y, 1)) //if there is a gun at x,y slot 2
        {
            if (environment.getGunType(x, y, 1).equals("Plasma Cannon")) //if the gun is a plasma cannon
            {
                drawPlasmaCannon(63, 0, exampleImage); //draw Plasma Cannon at 63,0
            }

            if (environment.getGunType(x, y, 1).equals("Chain Gun")) //if the gun is a chain gun
            {
                drawChainGun(63, 0, exampleImage); //draw ChainGun at 63,0
            }

            if (environment.getGunType(x, y, 1).equals("Pistol")) //if the gun is a pistol
            {
                drawPistol(63, 0, exampleImage); //draw Pistol at 63,0
            }
        }

        drawer.setColor(Color.WHITE); //set color to white for all lines

        if (environment.getDirection(x, y).equals("WEST")) //if the direction is WEST
        {
            drawer.drawLine(40, 50, 20, 50); //Left Line
        }
        if (environment.getDirection(x, y).equals("NORTH")) //if the direction is NORTH
        {
            drawer.drawLine(50, 20, 50, 40); //Up Line
        }
        if (environment.getDirection(x, y).equals("EAST")) //if the direction is EAST
        {
            drawer.drawLine(60, 50, 80, 50); //Right Line
        }
        if (environment.getDirection(x, y).equals("SOUTH")) //if the direction is SOUTH
        {
            drawer.drawLine(50, 60, 50, 80); //Down Line
        }

        if (environment.getLifeFormWeapon(x, y).equals("Plasma Cannon")) //if the weapon being held is a plasma cannon
        {
            return drawPlasmaCannon(55, 55, exampleImage); ////draw Plasma Cannon at 55,55
        }

        if (environment.getLifeFormWeapon(x, y).equals("Chain Gun")) //if the weapon being held is a chain gun
        {
            drawChainGun(55, 55, exampleImage); //draw Chain Gun at 55,55
        }

        if (environment.getLifeFormWeapon(x, y).equals("Pistol")) //if the weapon being held is a pistol
        {
            drawPistol(55, 55, exampleImage); //draw Pistol at 55,55
        }
        return new ImageIcon(exampleImage);
    }

    /**
     * Kyle Jones
     * @param exampleImage
     * @return the ImageIcon of a human
     */
    private ImageIcon drawHuman(BufferedImage exampleImage)
    {
        Graphics drawer = exampleImage.getGraphics();

        //Human
        drawer.setColor(new Color(0, 255, 0)); //set color to blue 
        drawer.fillOval(40, 40, 20, 20); //makes the humans

        return new ImageIcon(exampleImage);
    }

    /**
     * Kyle Jones
     * @param exampleImage
     * @return the ImageIcon of an alien
    */
    private ImageIcon drawAlien(BufferedImage exampleImage)
    {
        Graphics drawer = exampleImage.getGraphics();

        //Alien
        drawer.setColor(Color.ORANGE); //sets the color to orange
        drawer.fillRoundRect(40, 40, 20, 20, 20, 5);

        return new ImageIcon(exampleImage);
    }

    /**
     * Kyle Jones
     * @param exampleImage
     * @return the ImageIcon of a ChainGun
     */
    private ImageIcon drawChainGun(int x, int y, BufferedImage exampleImage)
    {
        Graphics drawer = exampleImage.getGraphics();

        //draw Chain Gun
        drawer.setColor(Color.YELLOW);
        drawer.fillOval(x, y, 35, 35); //ChainGun

        return new ImageIcon(exampleImage);
    }

    /**
     * Kyle Jones
     * @param exampleImage
     * @return the ImageIcon of a PlasmaCannon
     */
    private ImageIcon drawPlasmaCannon(int x, int y, BufferedImage exampleImage)
    {

        Graphics drawer = exampleImage.getGraphics();
        //Plasma Cannon
        drawer.setColor(Color.WHITE);
        drawer.drawOval(x, y, 35, 35);

        return new ImageIcon(exampleImage);
    }

    /**
     * Kyle Jones
     * @param exampleImage 
     * @return the ImageIcon of a Pistol
     */
    private ImageIcon drawPistol(int x, int y, BufferedImage exampleImage)
    {
        Graphics drawer = exampleImage.getGraphics();

        //draw Pistol
        drawer.setColor(Color.MAGENTA); //Pistol
        drawer.fillOval(x, y, 35, 35);

        return new ImageIcon(exampleImage);
    }

    /**
     * Update function since this is now implementing invokerObserver
     */
    @Override
    public void updateMap(Boolean updated)
    {
        if (updated == true)
            updateMap(Environment.getWorldInstance());

    }

}
