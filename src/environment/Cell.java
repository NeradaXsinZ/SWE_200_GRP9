/*
 * Programmer:Reginald Nettey
 * Professor:Dr. Girard 
 * Course:Design Patterns
 * Description:A Cell that can hold a LifeForm
 */
package environment;

import java.util.Vector;

import weapon.ChainGun;
import weapon.Pistol;
import weapon.PlasmaCannon;
import weapon.Weapon;
import lifeform.LifeForm;

public class Cell
{
    LifeForm entity;

    private Vector<Weapon> weapon = new Vector<Weapon>();

    /**
    * @return the LifeForm in this Cell
    */
    public LifeForm getLifeForm()
    {

        return entity;
    }

    /**
    * Tries to add the LIfeForm to the Cell.  Will not add if a LifeForm is
    * already present
    * @return true if the LifeForm was added the Cell, false otherwise
    */
    public boolean addLifeForm(LifeForm entity)
    {
        if (this.entity == null)
        {
            this.entity = entity;
            return true;

        }
        else
            return false;
    }

    /**
    * remove a life form from cell
    */
    public void removeLifeForm()
    {
        if (!(this.entity == null))
        {
            this.entity = null;
        }
    }

    /**
     * Adds a weapon to the cell
     * This Method was implemented with an arraylist so 
     * There was no need to test to see if one weapon could be
     * Placed in an occupied space
     * @param wep the weapon to be added
     * @return true if it was added, false otherwise
     * 
     */
    public boolean addWeapon(Weapon wep)
    {
        if (weapon.size() < 2)
        {
            weapon.add(wep);
            return true;
        }
        return false;

    }

    /**
     * Removes a weapon from the environment
     * @param wep the weapon to be removed
     * @return true if removed and false otherwise
     */
    public boolean removeWeapon(Weapon wep)
    {
        if (weapon.contains(wep))
        {
            return weapon.remove(wep);
        }
        return false;
    }

    /**
     * Gets the number of weapons in the cell
     * @return the number of weapons
     */
    public int getNumWeapons()
    {
        return weapon.size();
    }

    /**
     * 
     * @return the direction of the lifeform
     */
    public String getDirection()
    {
        if (entity != null)
        {
            return entity.getCurrentDirection();
        }
        return " ";

    }

    /**
     * 
     * @param i the weapon slot
     * @return	return the weapon in spot i
     */
    public Weapon getWeaponInCell(int i)
    {
        if (weapon.size() > i)
        {
            return weapon.get(i); //return weapon in spot i    		
        }
        return null;
    }

    /**
     * 
     * @return the weapon type that the LifeForm has
     */
    public String getLifeFormWeapon()
    {
        if (entity != null) //if there is a LifeForm in the cell
        {
            if (entity.wep != null) //if the weapon exists
            {
                if (entity.wep instanceof ChainGun) //the entity's weapon is instanceof ChainGun
                {
                    return "Chain Gun"; //ChainGun
                }
                else
                {
                    if (entity.wep instanceof PlasmaCannon)//the entity's weapon is instanceof PlasmaCannon
                    {
                        return "Plasma Cannon"; //Plasma Cannon
                    }
                    else
                    {
                        if (entity.wep instanceof Pistol) //the entity's weapon is instanceof Pistol
                        {
                            return "Pistol"; //Pistol
                        }
                    }
                }

            }
        }
        return " "; //nothing
    }

    /**
     * TODO Kyle Jones
     * @param i which weapon spot 
     * @return	the type of weapon (if any)
     */
    public String getGunType(int i)
    {
        if (weapon.get(i) != null)
        {
            if (weapon.get(i) instanceof ChainGun) //instance of ChainGun
            {
                return "Chain Gun"; //ChainGun
            }
            if (weapon.get(i) instanceof Pistol) //instance of Pistol
            {
                return "Pistol"; //Pistol
            }
            if (weapon.get(i) instanceof PlasmaCannon) //instance of PlasmaCannon
            {
                return "Plasma Cannon"; //Plasma Cannon
            }
        }

        return " "; //No Gun
    }

    //public Vector<Weapon> getWeapons()
    //{
    //return weapon;
    //}

}
