/*
 * Programmer:Reginald Nettey
 * Professor:Dr. Girard 
 * Course:Design Patterns
 * Description: Allows the alien to heal a percentage of their Hp
 */
package recovery;

public class RecoveryFractional implements RecoveryBehavior
{
    private double recoveryStep;

    /**
     * Allows creation of an instance of RecoveryFractional
     * 
     * @param recoveryStep the percentage of Max HitPoints recovered
     */
    public RecoveryFractional(double recoveryStep)
    {
        this.recoveryStep = recoveryStep;

    }

    /**
     * Calculate fractional recovery 
     * 
     * @param currentLife The current hit points of the cell
     * @param maxLife The max Hit points of the alien
     * @return the new current life
     */
    @Override
    public int calculateRecovery(int currentLife, int maxLife)
    {
        if (currentLife + (Math.ceil(maxLife * recoveryStep)) <= maxLife && currentLife > 0)
            return currentLife += Math.ceil(maxLife * recoveryStep);
        else if (currentLife <= 0)
            return currentLife = 0;
        return maxLife;
    }

}
