/*
 * Programmer:Reginald Nettey
 * Professor:Dr. Girard 
 * Course:Design Patterns
 * Description:Does not recover any hit points
 */
package recovery;

public class RecoveryNone implements RecoveryBehavior
{
    /**
     * Returns the current hit points of the aliens with no recovery
     * 
     * @param currentLife the current hit points 
     * @param maxLife the max hit points of the alien
     */
    @Override
    public int calculateRecovery(int currentLife, int maxLife)
    {
        return currentLife;
    }

}
