/*
 * Programmer:Reginald Nettey
 * Professor:Dr. Girard 
 * Course:Design Patterns
 * Description: Linear(set amount) recovery for an alien
 */
package recovery;

public class RecoveryLinear implements RecoveryBehavior
{

    private int recoveryStep;

    /**
     *Allows creation of an instance of RecoveryLinear
     * 
     * @param recoveryStep the amount to be recovered
     */
    public RecoveryLinear(int recoveryStep)
    {
        this.recoveryStep = recoveryStep;
    }

    /**
     * Calculates the new currentLife after life has been added
     * 
     * @param currentLife the current Hp of the alien
     * @param maxLife the max Hp of the alien
     * @return current life of the alien after calculation
     */
    @Override
    public int calculateRecovery(int currentLife, int maxLife)
    {
        if (currentLife + recoveryStep <= maxLife && currentLife > 0)
            return currentLife += recoveryStep;
        else if (currentLife <= 0)
            return 0;
        return maxLife;
    }
}
