/*
 * Programmer:Reginald Nettey
 * Professor:Dr. Girard 
 * Course:Design Patterns
 * Description:Interface for the 3 recovery behaviors
 */
package recovery;

public interface RecoveryBehavior
{
    /**
     * Method that has to be in all things that implement this interface
     * 
     * @param currentLife the current hit points
     * @param maxLife the maximum Hp of the alien
     * @return the new amount of hit points(currentHitPoints)
     */
    public int calculateRecovery(int currentLife, int maxLife);
}
