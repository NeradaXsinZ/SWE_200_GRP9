/*
 * Programmer:Reginald Nettey
 * Professor:Dr. Girard 
 * Course:Design Patterns
 * Description:Interface that contains update time method used by lifeforms
 */
package gameplay;

public interface TimeObserver
{
    /**
     * Updates when time is changed(round is incremented)	
     * @param time the current round
     */
    public void updateTime(int time);
}
