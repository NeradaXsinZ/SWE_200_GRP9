/**@author Reginald Nettey*/


package gameplay;

import recovery.RecoveryBehavior;
import recovery.RecoveryFractional;
import recovery.RecoveryLinear;
import recovery.RecoveryNone;
import state.AIContext;
import weapon.ChainGun;
import weapon.Pistol;
import weapon.PlasmaCannon;
import weapon.Weapon;
import environment.Environment;

import java.util.ArrayList;
import java.util.Random;

import lifeform.Alien;
import lifeform.Human;
import lifeform.LifeForm;

public class Simulator implements TimeObserver
{
	Environment env = Environment.getWorldInstance();
	Random rand = new Random();
	LifeForm lf;
	RecoveryBehavior recovery;
	ArrayList <AIContext> ai=new ArrayList<AIContext>();
	int round;
	
	/**
	 * Constructor for simulator which calls on the populate method
	 * @param numHumans
	 * @param numAliens
	 */
	public Simulator(int numHumans, int numAliens)
	{
	
		populateMap(numHumans,numAliens);
	}
	/**
	 * This method populates the environment with humans, aliens and weapons
	 * @param humans the number of humans to be added
	 * @param aliens the number of alien to be added
	 */
	private void populateMap(int humans,int aliens)
	{
		int nh=0,na=0,hp,armor=0,whichRecov,amtRecov,wepType, recRate,randX =0,randY=0,randWX=0,randWY=0;


		Boolean flag = true;
		ArrayList <LifeForm>  lfArr = new ArrayList<LifeForm>();
		ArrayList <Weapon> wepArr = new ArrayList<Weapon>();
		for(int i=0;i< humans;i++)
		{
			hp = rand.nextInt(41)+10;	//Sets hp from 10-50
			armor = rand.nextInt(8)+2;	//Sets armor from 2-9
			lfArr.add(new Human("Human"+i,hp,armor));
			ai.add( new AIContext(lfArr.get(i)));
			nh++;
		}
		for(int j=lfArr.size(); j < (humans+aliens); j++)
		{
			hp = rand.nextInt(31)+10;	//Sets alien hp from 10-40 
			recRate = rand.nextInt(3)+2; //Sets recovery rate form 2-4	
			whichRecov = rand.nextInt(3);
			switch(whichRecov)
			{
				case 0:
					recovery = new RecoveryNone();
					break;
				case 1:
					amtRecov = rand.nextInt(6)+3;	//Recovers from 3-8 heath every 2 rounds
					recovery = new RecoveryLinear(amtRecov);
					break;
				case 2:
					amtRecov = rand.nextInt(9)+2;	//Recovers from 2-10% heath every 2 rounds
					recovery = new RecoveryFractional(amtRecov);
					break;
			}
			lfArr.add(j, new Alien("Alien"+(j-humans),hp,recovery,recRate));
			ai.add( new AIContext(lfArr.get(j)));

			na++;
		}
    	for(int w=0; w < (humans+aliens); w ++)
    	{
    		wepType = rand.nextInt(3);	//sets wepType from 0-2
    		if(wepType==0)
    			wepArr.add(new Pistol());
    		else if (wepType==1)
    			wepArr.add(new ChainGun());
    		else if (wepType==2)
    			wepArr.add(new PlasmaCannon());
    	}
    	
    	for(int j=0; j < wepArr.size(); j++)
    	{					   
    		while(flag)
			{
				if(wepArr.size()<=1)
					flag=false;

				randWX = rand.nextInt(env.getMaxRow());
				randWY = rand.nextInt(env.getMaxCol());	
				
				try{
			    		System.out.println(wepArr.get(j)+": added to: ("+randWX+","+randWY+")");
						env.addWeaponToEnvironment(randWX, randWY, wepArr.get(j));
						wepArr.remove(j);
					}
				catch(NullPointerException e)
				{
				    System.out.print("Something Was Null");
				}
			}
    		j--;
    	}
    	flag=true;
		for(int i=0; i < lfArr.size();i++)
		{
			
			
			while(flag)
			{
				if(lfArr.size()<=1)
					flag=false;
				try
				{
					randX = rand.nextInt(env.getMaxRow());
					randY = rand.nextInt(env.getMaxCol());
					
					if(env.getCellArr()[randX][randY]==null|| env.getCellArr()[randX][randY].getLifeForm()==null)
					{
				    	env.addLifeFormToEnvironment(randX, randY, lfArr.get(i));

				    	System.out.println(lfArr.get(i).getName()+": added to: ("+randX+","+randY+")");
				    	lfArr.remove(i);
					}	
				}
				catch(IndexOutOfBoundsException e)
				{
					System.out.println("Out of bounds: "+randX+ ","+randY);
				}
			}	
			i--;
		}
		
	System.out.println(env.getMaxRow()+","+env.getMaxCol());
		
	}
	/**
	 * updates every time there is a new round
	 * @param time-the current round
	 */
	@Override
	public void updateTime(int time)
	{
		round = time;
		for(AIContext cont: ai)	
			cont.setRound(time);
 	}
	
}
