/*
 * Programmer:Reginald Nettey
 * Professor:Dr. Girard 
 * Course:Design Patterns
 * Description:SimpleTimer class that that allows observers to be added
 */
package gameplay;

import java.util.Vector;

public class SimpleTimer extends Thread implements Timer
{
    private int round, waitTime;

    private Vector<TimeObserver> theObservers = new Vector<TimeObserver>();

    boolean flag = true;

    /**
     * Constructor of SimpleTimer with wait time
     * allows creation of an instance of SimpleTimer
     * @param waitTime the time to wait(for each round)
     */
    public SimpleTimer(int waitTime)
    {
        this.waitTime = waitTime;
        round = 0;
    }

    /**
     * A SimpleTimer constructor without parameters
     */
    public SimpleTimer()
    {
        round = 0;
    }

    /**
     * Allows addition of an observer to theObservers vector
     */
    public void addTimeObserver(TimeObserver observer)
    {
        theObservers.add(observer);
    }

    /**
     * Allows removal of an observer from theObservers vector
     */
    public void removeTimeObserver(TimeObserver observer)
    {
        theObservers.remove(observer);
    }

    /**
     * Changes the time(increments round)/calls updateTime(int)
     */
    public void timeChanged()
    {
        round++;
        if (!obsvIsEmpty())
            theObservers.get(0).updateTime(round);
    }

    /**
     * Accessor for round
     * @return the current round
     */
    public int getRound()
    {
        return round;
    }

    /**
     * Checks to see if the observer is Empty
     * @return true if theObservers is empty otherwise false
     */
    public boolean obsvIsEmpty()
    {

        if (theObservers.isEmpty())
            return true;
        else
            return false;
    }

    /**
     * Overridden run method to automatically change rounds and 
     * update updateTime method
     */
    @Override
    public void run()
    {
        try
        {
            while (flag)
            {
                sleep(waitTime);
                timeChanged();

            }
        }
        catch (InterruptedException e)
        {
            System.out.print("Something went wrong");
        }
    }

    /**
     * Stops the thread
     */
    public void stopThread()
    {
        flag = false;
    }

}
