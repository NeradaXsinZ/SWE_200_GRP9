/*
 * Programmer:Reginald Nettey
 * Professor:Dr. Girard 
 * Course:Design Patterns
 * Description:Interface that contains the methods implemented in SimpleTimer
 */
package gameplay;

public interface Timer
{
    /**
     * Allows observer to be added
     * @param observer
     */
    public void addTimeObserver(TimeObserver observer);

    /**
     * Allows observer to be removed
     * @param observer
     */
    public void removeTimeObserver(TimeObserver observer);

    /**
     * changes the time
     */
    public void timeChanged();

    /**
     * 
     * @return the current round
     */
    public int getRound();

    /**
     * Checks if the vector is empty(no observers)
     * @return true if empty false otherwise
     */
    public boolean obsvIsEmpty();
}
