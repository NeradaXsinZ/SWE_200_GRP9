package state;

import lifeform.LifeForm;
import state.NoWeapon;
/**
 * 
 * @author Kyle Jones
 * AIContext calls the current states evaluation
 * Has a state which is where the lifeform the AIContext is controlling is stored
 */
public class AIContext
{

	private ActionState state;		//the current state
	private int time;		//the cuurent time
	
	
	/**
	 * Create the AIContext for the lifeform
	 * @param lifeform		the lifeform that ai is controlling
	 */
	public AIContext(LifeForm lifeform)
	{
		state = evaluateState(lifeform);		//evaluate the state of the lifeform and store it
	}
	
	/**
	 * Evaluates which state the lifeform should be in
	 * @param lifeform the lifeform that is being evaluated
	 * @return	the state that the AIContext will be
	 */
	private ActionState evaluateState(LifeForm lifeform)
	{
		if(lifeform.getCurrentLifePoints() > 0)	//is it alive
		{	
			if(lifeform.getWeapon() != null)	//if there is a weapon
			{
				if(lifeform.getWeapon().getCurrentAmmo() > 0)		//there is still ammo
				{
					return new HasWeapon(lifeform,this);		//has weapon state 
				}else								//no ammo
				{
					return new OutOfAmmo(lifeform,this);	//OutOfAmmo state
				}
			}
			return new NoWeapon(lifeform,this);		//no weapon state
		}else
		{
			return new Dead(lifeform, this);	// dead state
		}
	}
	
	/**
	 * set a new state
	 * @param state the new state
	 */
	public void setState(ActionState state)
	{
		this.state = state;	//set the state passed in
	}
	
	/**
	 * evaluate the current state
	 */
	public void execute()
	{
		state.evaluation();		//call the current state
	}
	
	/**
	 * get the state
	 * @return the current state
	 */
	public ActionState getState()
	{
		return state;		//return the state
	}
	
	/**
	 * Regi
	 * Sets round time and execute for the current AIContext
	 * @param time the current time
	 */
	public void setRound(int time)
	{
		execute();		// execute for the AIContext
		this.time = time;	//set time 
	}
	
	/**
	 * Regi
	 * get the current time
	 * @return the time
	 */
	public int getRound()
	{
		return time;		//return time
	}

}
