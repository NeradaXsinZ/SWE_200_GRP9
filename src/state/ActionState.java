package state;

import environment.Environment;
import lifeform.LifeForm;
/**
 * 
 * @author Kyle Jones
 * The abstract class for states
 * All states will have have an evaluation method, a LifeForm, an Environment, and an AIContext
 */
public abstract class ActionState
{
    LifeForm entity;		//the lifeform the state is controlling

    Environment environment = Environment.getWorldInstance();		//the environment

    AIContext ai;		//the AIContext that this state belongs to 
	
    /**
     * Stores instance variables that all the states have
     * @param lifeform	the lifeform it is controlling 
     * @param aiContext	the AIContext that is storing this state
     */
	public ActionState(LifeForm lifeform, AIContext aiContext)
	{
		entity = lifeform;		//the lifeform it is controlling 
		ai = aiContext;		//the AIContext that is storing this state
	}
	
	/**
	 * 	Each ActionState must have an evaluation to call when the AIContext is in that state
	 */
	public abstract void evaluation();	//evaluates the current state
	

    
	
}
