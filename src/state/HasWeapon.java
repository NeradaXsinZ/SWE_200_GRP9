/**
 * @TODO Tim Haus
 */
package state;

import java.util.Random;

import weapon.Weapon;
import lifeform.LifeForm;
import environment.Environment;

//Action state of has weapon    
public class HasWeapon extends ActionState 
{

    LifeForm enemy;

    Random rand = new Random(); //Creates a random object number

    //constructor that takes the lifeform and the context
    public HasWeapon(LifeForm lifeform, AIContext ai)
    {
        super(lifeform, ai);
    }

    //Starts the method for evaluating if a target can attack and if not will search
    //@TODO ATTENTION: THERE WILL BE AN OUT OF BOUNDS EXCEPTION BECAUSE ONE MEMBER OF THE GROUP DID NOT PUT BOUNDERIES IN THE MOVEMENT METHOD
    public void evaluation()
    {
        int x = entity.getXLoc(); //Makes x the x cordinate that the lifeform is in
        int y = entity.getYLoc(); //Makes y the y cordinate that the lifeform is in

        if (entity.getCurrentDirection() == "NORTH") //if the direction is north go into if statement
        {
            int r = x, c = y - 1; //makes temp variables of the coordinates
            while (c >= 0) //while the collum is greater than 0
            {

                if (environment.getLifeFormType(r, c) != " " && environment.getLifeFormType(x, y) != environment.getLifeFormType(r, c)) //If the environment has a lifeform of a different type
                {
                    attackTarget(r, c); //attack the target
                    break;
                }

                c--;

            }
            search(); //calls on the search method
        }

        if (entity.getCurrentDirection() == "WEST") //SAME COMMENTS AS NORTH BUT FOR THE DIFFERENT DIRECTIONS
        {
            int r = x - 1, c = y;
            while (r >= 0)
            {

                if (environment.getLifeFormType(r, c) != " " && environment.getLifeFormType(x, y) != environment.getLifeFormType(r, c))
                {
                    attackTarget(r, c);
                    break;
                }

                r--;

            }
            search();
        }

        if (entity.getCurrentDirection() == "EAST") //SAME COMMENTS AS NORTH BUT FOR THE DIFFERENT DIRECTIONS
        {
            int r = x + 1, c = y;
            while (r < environment.getMaxRow())
            {

                if (environment.getLifeFormType(r, c) != " " && environment.getLifeFormType(x, y) != environment.getLifeFormType(r, c))
                {
                    attackTarget(r, c);
                    break;
                }

                r++;

            }
            search();
        }

        if (entity.getCurrentDirection() == "SOUTH") //SAME COMMENTS AS NORTH BUT FOR THE DIFFERENT DIRECTIONS
        {
            int r = x, c = y + 1;
            while (c < environment.getMaxCol())
            {

                if (environment.getLifeFormType(r, c) != " " && environment.getLifeFormType(x, y) != environment.getLifeFormType(r, c))
                {
                    attackTarget(r, c);
                    break;
                }

                c++;

            }
            search();

        }

    }

    //method that attacks the specific target
    public void attackTarget(int row, int col)
    {
        Weapon gun = entity.getWeapon(); //makes a gun that the entity is holding
        enemy = environment.getLifeForm(row, col); //enemy is now the targeted lifeform
        entity.attack(enemy); //entity attacks its enemy
        
        if (gun.getCurrentAmmo() <= 0) //if the gun has no ammo
        {
            OutOfAmmo ooa = new OutOfAmmo(entity, ai);     //ooa1 (OutOfAmmo) bob
            ai.setState(new OutOfAmmo(this.entity, ai)); //set the state to out of ammo
            ooa.evaluation();
        }
    }

    //method for if the lifeform is dead
    public void dead()
    {
        Dead guy = new Dead(entity, ai);
        ai.setState(new Dead(this.entity, ai)); //sets the state to dead
        guy.evaluation();
    }

    //method for searching for a new way to move and turn at random
    public void search()
    {
        int random = rand.nextInt(4); //make a random number eather, 1,2,3 or 4

        if (random == 0) //if the number is one
        {
            entity.turn("NORTH"); //turn north 
            environment.move(entity); //move in that direction
        }
        if (random == 1) //if the number is two
        {
            entity.turn("EAST"); //turn east
            environment.move(entity); //move in that direction
        }
        if (random == 2) //SAME AS PREVIOUS COMMENTS BUT WITH DIFFERENT DIRECTIONS AND NUMBERS
        {
            entity.turn("SOUTH");
            environment.move(entity);
        }
        if (random == 3)
        {
            entity.turn("WEST");
            environment.move(entity);
        }

    }

    

    

    


	
    
}
