//RYAN HANDLEY
package state;

import environment.Cell;
import environment.Environment;
import lifeform.LifeForm;
import java.util.Random;


public class NoWeapon extends ActionState
{
	Environment env;
	Random rand = new Random();
	

    public NoWeapon(LifeForm lifeform, AIContext aiContext)
    {
        super(lifeform, aiContext);
        env = Environment.getWorldInstance();
    }

    /**
     * if the lifeform isn't dead, it will check to see if there is a weapon in the cell it current resides in. 
     * if there is no weapon in the current cell, it will move to a random cell
     */
	@Override
	public void evaluation()
	{
		int xpos = entity.getXLoc();
		int ypos = entity.getYLoc();
		
		if(entity.getCurrentLifePoints() == 0) // if the lifeform has zero life points, we switch to the dead state
		{
			deadState();
		}
		else
		{
			if(env.isThereAGun(xpos,ypos,0) == true)	//if there's a gun in the cell, the lifeForm picks it up
			{
				acquireWeapon();
			}
			else
			{
				search();					//if there's no weapon in the cell, the lifeForm will search
			}
		}
	}
	

    /**
     * sets the lifeform to the dead state
     */
	public void deadState()
	{
		ai.setState(new Dead(entity,ai));
	}
	
	/**
	 * acquires a weapon if there is one present in the current cell and
	 * then switches the lifeForms state to hasWeapon
	 */
	public void acquireWeapon()
	{
		int xpos = entity.getXLoc();
		int ypos = entity.getYLoc();
		entity.addWeapon(env.getCellArr()[xpos][ypos].getWeaponInCell(0));
		env.removeWeaponFromEnvironment(xpos,ypos,entity.getWeapon());
		hasWeapon();
	}
	
	/**
	 * changes the lifeforms state to hasweapon
	 */
	public void hasWeapon()
	{
		ai.setState(new HasWeapon(entity,ai));
	}
	
	/**
	 * the entity will randomly pick a direction and move 
	 */
	public void search()
	{
		int randomNumber = rand.nextInt(4);
		
		if(randomNumber == 1)
		{
			entity.turn("WEST");
			env.move(entity);
		}
		if(randomNumber == 2)
		{
			entity.turn("SOUTH");
			env.move(entity);
		}
		if(randomNumber == 3)
		{
			entity.turn("EAST");
			env.move(entity);
		}
		if(randomNumber == 4)
		{
			entity.turn("NORTH");
			env.move(entity);
		}
		
	}

}
