package state;

import java.util.Random;

import environment.Environment;

import weapon.Weapon;
import lifeform.LifeForm;

/**
 * The dead state for when the lifeform is dead
 * @author David Burkett
 *
 */
public class Dead extends ActionState
{

    Random rand = new Random();
    
    int randx;

    int randy;

    Environment env = Environment.getWorldInstance();

    /**
     * A constructor for the dead state
     * @param lifeform the lifeform you wish to be dead
     * @param ai the ai you want to use
     */
    public Dead(LifeForm lifeform, AIContext ai)
    {
        super(lifeform, ai);

    }

    /**
     * Evaluation method for the dead state
     */
    public void evaluation()
    {

        if (entity.getCurrentLifePoints() <= 0)
        {
            respawn();
        }
    }

    /**
     * Respawns at the start of the next round
     */
    public void respawn()
    {

        if (entity.wep != null)
        {
            Weapon wep;
            wep = entity.wep;
            boolean flag = true;
            entity.dropWeapon();
            while (flag)
            {
                randx = rand.nextInt(env.getMaxRow());
                randy = rand.nextInt(env.getMaxCol());
                if (env.getCellArr()[randx][randy] == null || env.getCellArr()[randx][randy].getNumWeapons() < 2)
                {
                    env.addWeaponToEnvironment(randx, randy, wep);
                    flag = false;
                }
            }

        }

        env.removeLifeFormFromEnvironment(entity.getXLoc(), entity.getYLoc());
        boolean flag2 = true;
        while (flag2)
        {
            randx = rand.nextInt(env.getMaxRow());
            randy = rand.nextInt(env.getMaxCol());
            if (env.getCellArr()[randx][randy] == null || env.getCellArr()[randx][randy].getLifeForm() == null)
            {
                env.addLifeFormToEnvironment(randx, randy, entity);
                entity.setToMaxHealth();
                ai.setState(new NoWeapon(entity, ai));
                flag2 = false;
            }
        }

    }

}
