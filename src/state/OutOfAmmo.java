package state;

import lifeform.LifeForm;
/**
 * 
 * @author Kyle Jones
 *	A state for lifeforms that have no ammo but do have a gun
 */
public class OutOfAmmo extends ActionState
{
	/**
	 * 
	 * @param lifeform	the lifeform for the state
	 * @param aiContext	the ai context for the state
	 */
	public OutOfAmmo(LifeForm lifeform, AIContext aiContext)
	{
		super(lifeform, aiContext);		//pass lifeform and aiContext to Action state to store instance variables
	}
	
	/**
	 * Evaluate the entity's currentLifePoints, gun, and ammo and act accordingly
	 */
	@Override
	public void evaluation()
	{
		if (entity.getCurrentLifePoints() > 0)
		{	
			if(entity.getWeapon() != null)		
			{
				if (entity.getWeapon().getCurrentAmmo() <= 0)
				{
					reload();		//reload 
					hasWeapon();	//it has ammo so change state to HasWeapon
				}else
				{
					hasWeapon();		//it has ammo so change state to HasWeapon
				}
			}else
			{
				noWeapon();		//change state to noWeapon because the entity has no Weapon
			}
		}else
		{
			dead();		//change state to dead
		}
	}
	/**
	 * Changes the AI state to NoWeapon with the same
	 * lifeform and ai
	 */
	private void noWeapon()
	{
		ai.setState(new NoWeapon(entity, ai));
	}
		
	/**
	 * Changes the AI sate to HasWeapon 
	 */
	private void hasWeapon()
	{
		ai.setState(new HasWeapon(entity, ai));		//set  the ai state to HasWeapon with entity and ai
	}
	
	/**
	 *	Changes the state of the AI to Dead 
	 */
	private void dead()
	{
		ai.setState(new Dead(entity, ai)); 		//set  the ai state to Dead with entity and ai
	}

    /**
     * Reload the gun for this LifeForm
     */
	private void reload()
	{
		entity.getWeapon().reload();		//reload the entity's weapon
	}
	
	

}
