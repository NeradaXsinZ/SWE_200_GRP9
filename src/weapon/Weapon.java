package weapon;

/**
 * Interface for all the functions related to weapons.
 * @author David Burkett (Disco-Dave)
 */
public interface Weapon
{

    /**
     * Calculates the damage to be done with a weapon, and
     * returns the updated Hit Points to the victim Lifeform.
     * 
     * @return The updated Hit Points for a lifeform
     */
    public int calculateDamage();

    /**
     * A simple functions that sets the guns current ammo equal
     * to maximum ammo for the weapon
     */
    public void reload();

    /**
     * A simple getter to return the distance of between
     * two different lifeforms.
     * 
     * @return The range in between two lifeforms 
     */
    public int getRange();

    /**
     * A simple setter for setting the range between two
     * lifeforms
     * 
     * @param currRange: The new range that the user wishes
     * to set.
     */
    public void setRange(int currRange);

    /**
     * Getter for The current ammo of a weapon
     * @return currAmmo: the current ammo of the weapon
     */
    public int getCurrentAmmo();

    /**
     * Getter for the max ammo of a weapon
     * @return maxAmmo: the maximum ammo of the weapon;
     */
    public int getMaxAmmo();

    /**
     * Return the maximum range
     * @return maxRange: the maximum Range of the weapon;
     */
    public int getMaxRange();

    /**
     * get the number of times left that weapon can be wrapped
     * @return getWrapped: the number of times left to wrap
     */
    public int getWrapped();

    /**
     * Decrements numToWrap
     */
    public void Wrap();

    /**
     * Start a new round, allowing for more shots based on the rate of fire.
     */
    public void newRound();
    


}
