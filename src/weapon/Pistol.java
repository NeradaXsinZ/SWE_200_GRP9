package weapon;

/**
 * Contains the concrete functions for pistol
 * @author David Burkett (Disco-Dave)
 *
 */
public class Pistol extends GenericWeapon
{

    /**
      * A constructor that sets all the variables
      * needed for pistol.
      */
    public Pistol()
    {
        rateOfFire = 2;
        baseDamage = 10;
        maxRange = 50;
        currRange = 0;

        maxAmmo = 10;
        currAmmo = maxAmmo;

    }

    /**
     * Calculates damage to be inflicted.
     * @return Damage to be inflicted
     */
    @Override
    public int calculateDamage()
    {
        if ((currAmmo <= 0) || (shotsFired >= rateOfFire))
        {
            return 0;
        }
        if (currRange > maxRange)
        {
            currAmmo--;
            shotsFired++;
            return 0;
        }
        currAmmo--;
        shotsFired++;
        return ((baseDamage) * ((maxRange - currRange + 10)) / (maxRange));
    }

}
