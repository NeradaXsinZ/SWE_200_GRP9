package weapon;

/**
 * The Scope lets a Weapon be able to shoot its target
 * from 10 feet further than the maxRange of that
 * Weapon without changing the maxRange of the Weapon
 * (for the sake of calculations).
 * @author  Jeff Titanich, Reginad Nettey
 */
public class Scope extends Attachment
{
    /**
     * Constructor for scope which calls the add method in Attachment
     * @param w: a variable of type weapon
     */
    public Scope(Weapon w)
    {
        add(w);
    }

    /**
     *calculatDamage() for scope override superclass
     * @return the calculated damage
     */
    @Override
    public int calculateDamage()
    {
        if ((getRange() - 10) <= wep.getMaxRange() && getRange() >= 0)
        {
            return (int) (wep.calculateDamage() * (1 + (getRange() / (double) wep.getMaxRange())));
        }
        else
            return 0;
    }

    
}
