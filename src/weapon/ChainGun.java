/**
 * @author Reginald Nettey
 *
 *Code for a chainGun of type Generic weapon
 */
package weapon;

public class ChainGun extends GenericWeapon
{

    /**
     * The constructor for chain gun... 
     * sets all the variables  declared in the 
     * superclass(GenericWeapon)
     */
    public ChainGun()
    {
        rateOfFire = 4;
        baseDamage = 15;
        maxAmmo = 40;
        maxRange = 60;
        currAmmo = maxAmmo;
        currRange = 0;
    }

    /**
     * The overridden calculateDamage method
     * This method also decrements form currAmmo
     * @return the calculated damage of the weapon
     */
    @Override
    public int calculateDamage()
    {
        if (currRange < 0 || rateOfFire <= shotsFired)
            currRange = 0;
        if (currAmmo > 0)
            currAmmo--;
        return (int) (baseDamage * ((currRange) / (double) (maxRange)));
    }

}
