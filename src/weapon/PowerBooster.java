/**
 * @author Reginald Nettey
 */
package weapon;

public class PowerBooster extends Attachment
{
    /**
     * Creates a powerbooster	
     * @param wep
     */
    public PowerBooster(Weapon w)
    {
        add(w);
    }

    /**
     * calculate damage for powerbooster
     * @return the calculated damage
     */
    @Override()
    public int calculateDamage()
    {
        double num = (1 + (wep.getCurrentAmmo() / (double) wep.getMaxAmmo()));
        return (int) (wep.calculateDamage() * num);

    }

   

}
