package weapon;

/**
 * Holds the concrete methods for the PlasmaCannon Weapon.
 * @author Jeff Titanich
 *
 */
public class PlasmaCannon extends GenericWeapon
{
    public PlasmaCannon()
    {
        baseDamage = 50;
        maxAmmo = 4;
        maxRange = 40;
        rateOfFire = 1;
        currAmmo = maxAmmo;
        currRange = 0;
        shotsFired = 0;
    }

    /**
     * Calculate the amount of damage the plasma cannon should do
     * based on the current amount of ammo left. It will only do
     * damage if the current range is less than the max range of
     * the weapon.
     * @return - the damage at the current range and ammo.
     */
    @Override
    public int calculateDamage()
    {
        if (shotsFired < rateOfFire && currAmmo > 0)
        {
            if (currRange > maxRange)
            {
                shotsFired++;
                currAmmo--;
                return 0;
            }
            double ammo = currAmmo;
            currAmmo--;

            shotsFired++;
            return (int) ((baseDamage) * (ammo / (maxAmmo)));
        }
        return 0;

    }

    /**
     * Reload the plasma cannon's ammo count.
     */
    @Override
    public void reload()
    {
        currAmmo = maxAmmo;
    }

    /**
     * @return - the current range of the plasma cannon.
     */
    @Override
    public int getRange()
    {
        return currRange;
    }

    /**
     * change the current range of the plasma cannon.
     */
    @Override
    public void setRange(int currRange)
    {
        this.currRange = currRange;
    }

    /**
     * @return - the base damage of a plasma cannon.
     */
    public int getBaseDamage()
    {
        return baseDamage;
    }

    /**
     * @return - the number of shots a plasma cannon can
     * shoot per round.
     */
    public int getRateOfFire()
    {
        return rateOfFire;
    }

    /**
     * @return - the largest amount of ammo a plasma cannon can have.
     */
    public int getMaxAmmo()
    {
        return maxAmmo;
    }

    /**
     * @return - the farthest distance the target is allowed to be from
     * the plasma cannon for it to deal damage.
     */
    public int getMaxRange()
    {
        return maxRange;
    }

    /**
     * Start a new round, allowing for more shots based on the rate of fire.
     */
    @Override
    public void newRound()
    {
        shotsFired = 0;
    }

    /**
     * Getter for current ammo;
     * @return current ammo
     */
    @Override
    public int getCurrentAmmo()
    {
        return currAmmo;
    }
}
