/**
 * @author Reginald Nettey
 * An abstract class for attachments
 * 
 *
 */
package weapon;

public abstract class Attachment implements Weapon
{
    Weapon wep;

    Boolean one;

    /**
     *This method has to be defined because it implements weapon,
     *however this method should never be used.
     */
    @Override
    public int calculateDamage()
    {
        return 0;
    }

    /**
     *This method has to be defined because it implements weapon,
     *however this method should never be used.
     */
    @Override
    public void reload()
    {

    }

    /**
     *This method has to be defined because it implements weapon,
     *however this method should never be used.
     */
    @Override
    public int getRange()
    {
        return wep.getRange();
    }

    /**
     *This method has to be defined because it implements weapon,
     *however this method should never be used.
     */
    @Override
    public void setRange(int currRange)
    {
        wep.setRange(currRange);
    }

    /**
     *This method has to be defined because it implements weapon,
     *however this method should never be used.
     */
    @Override
    public int getCurrentAmmo()
    {
        return wep.getCurrentAmmo();
    }

    /**
     * Getter for max ammo
     * @return the max ammo
     */
    @Override
    public int getMaxAmmo()
    {
        return wep.getMaxAmmo();
    }

    /**
     * Getter for max range
     * @return the max range
     */
    @Override
    public int getMaxRange()
    {
        return wep.getMaxRange();
    }

    /**
     * Getter for numToWrap
     * @return the number of times left to wrap
     */
    @Override
    public int getWrapped()
    {
        return wep.getWrapped();
    }

    /**
     * Decrements numToWrap in the GenericWeapon 
     */
    @Override
    public void Wrap()
    {
        wep.Wrap();

    }

    /**
     * Start a new round, allowing for more shots based on the rate of fire.
     */
    @Override
    public void newRound()
    {
        wep.newRound();
    }

    /**
     * Allows a weapon to be added and wrapped 2 time
     * @param w
     */
    protected void add(Weapon w)
    {
        try
        {

            if (w.getWrapped() > 0)
            {
                wep = w;
                w.Wrap();
            }
        }
        catch (NullPointerException e)
        {
        }

    }

}
