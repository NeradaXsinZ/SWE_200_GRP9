package weapon;

/**
 * @author Reginald Netty
 * GenericWeapon, will hold all the common methods and variables used in weapon.
 * 
 * 
 *
 */
public abstract class GenericWeapon implements Weapon
{
    /**
     * Declare Instance Vars
     */
    public int rateOfFire;

    public int baseDamage;

    public int maxRange;

    public int shotsFired;

    public int currAmmo;

    public int maxAmmo;

    public int numToWrap = 2;

    public int currRange;

    public int calculateDamage()
    {
        return 0;
    }

    /**
     * Reloads the ammo for the weapon, by simply setting
     * the variable currAmmo equal to maxAmmo.
     */
    @Override
    public void reload()
    {
        currAmmo = maxAmmo;

    }
    
   
    /**
     * A simple getter that will return the current range
     * between two lifeforms
     * 
     * @return currRange: the current range between two lifeforms
     */
    @Override
    public int getRange()
    {
        return currRange;
    }

    /**
     * Sets the range between two lifeforms
     * @params currRange: the range that you wish to set
     */
    @Override
    public void setRange(int currRange)
    {
        if (currRange < 0)
            currRange = 0;
        this.currRange = currRange;
    }

    /**
     * Refreshes the shots fired
     */
    public void newRound()
    {
        shotsFired = 0;
    }

    /**
     * Gets the current ammo
     * @return currAmmo: the current amount of ammo
     */
    public int getCurrentAmmo()
    {
        return currAmmo;
    }

    public void setCurrentAmmo(int num)
    {
        currAmmo = num;
    }

    /**
     * Get the maximum amount of ammo a weapon can hold
     * @return maxAmmo: the max amount of ammo
     */
    public int getMaxAmmo()
    {
        return maxAmmo;
    }

    /**
     * Gets max range of a weapon
     * @return maxRange: the maximum range of a weapon
     */
    public int getMaxRange()
    {
        return maxRange;
    }

    /**
     * A getter for numToWrap
     * @return numToWrap: the number of times left to wrap
     */
    public int getWrapped()
    {
        if (numToWrap < 0)
            numToWrap = 0;
        return numToWrap;
    }

    /**
     * Decrements num to wrap
     */
    public void Wrap()
    {
        if (numToWrap < 0)
            numToWrap = 0;
        numToWrap--;
    }
    
   

}
