package weapon;

/**
 * Functions for stabalizer attatchment, this will
 * reload the gun when it runs out of ammo and increases
 * the damage by 25%
 * 
 * @author David Burkett (Disco-Dave)
 *
 */
public class Stabilizer extends Attachment
{
    /**
     * A constructor that allows wrapping of other objects
     * that use the weapon interface, ie: Attachments and Weapons.
     * 
     * @param w A weapon or attachment
     */
    public Stabilizer(Weapon w)
    {
        add(w);
    }

    /**
     * Calculates new damage
     */
    @Override
    public int calculateDamage()
    {
        if (wep.getCurrentAmmo() <= 0)
            wep.reload();

        return (int) (wep.calculateDamage() * 1.25);
    }

  

}
