//RYAN HANDLEY
package ui.command;

import environment.Environment;
import lifeform.LifeForm;

public class Attack implements Command
{
    Environment env;

    LifeForm lf;

    int x, y;

    /**
     * constructor
     * @param lf
     */
    public Attack(LifeForm lf)
    {
        this.lf = lf;
        x = lf.getXLoc();
        y = lf.getYLoc();
        env = Environment.getWorldInstance();
    }

    /**
     * based on which direction the lifeForm is facing, it will search ahead
     * in the environment. If a lifeForm sees another lifeForm, it will 
     * attack it
     */
    public void performCommand()
    {
        int minCol = 0;
        int minRow = 0;
        x = lf.getXLoc();
        y = lf.getYLoc();
        if (lf.getCurrentDirection().equals("NORTH"))
        {
            int curx = x;
            int cury = y;
            while (curx > 0)
            {
                try
                {
                    lf.attack(env.getCellArr()[curx][cury].getLifeForm());
                }
                catch (NullPointerException e)
                {
                    System.out.println("Something Was Null");
                }
                curx--;
            }
        }
        else if (lf.getCurrentDirection().equals("EAST"))
        {
            int curx = x;
            int cury = y;
            if (env != null)
                while (cury < env.getMaxCol() - 1)
                {

                    try
                    {
                        lf.attack(env.getCellArr()[curx][cury].getLifeForm());
                    }
                    catch (NullPointerException e)
                    {
                        System.out.println("Something Was Null");
                    }
                    cury++;
                }
        }
        else if (lf.getCurrentDirection().equals("SOUTH"))
        {
            int curx = x;
            int cury = y;
            while (curx < env.getMaxRow() - 1)
            {
                try
                {
                    lf.attack(env.getCellArr()[curx][cury].getLifeForm());
                }
                catch (NullPointerException e)
                {
                    System.out.println("Something Was Null");
                }
                curx++;
            }
        }
        else if (lf.getCurrentDirection().equals("WEST"))
        {
            int curx = x;
            int cury = y;
            while (curx > minRow)
            {
                try
                {
                    lf.attack(env.getCellArr()[curx][cury].getLifeForm());
                }
                catch (NullPointerException e)
                {
                    System.out.println("Something Was Null");
                }
                curx--;

            }
        }
    }

}
