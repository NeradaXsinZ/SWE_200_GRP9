/**
 * @author Regi Nettey
 */
package ui.command;

/**Interface for the observer pattern... holds the update function*/
public interface InvokerObserver
{
    public void updateMap(Boolean updated);

}
