/**@author Reginald Nettey*/
package ui.command;

import java.util.ArrayList;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.event.KeyEvent;
import java.awt.event.KeyListener;
import javax.swing.*;
import lifeform.LifeForm;

/**Invoker Gui*/
public class Invoker implements KeyListener
{
    InvokerObserver observer;

    LifeForm lf;

    ArrayList<Command> comQ = new ArrayList<Command>();

    int acI, atI, drI, reI, moI, tuI;

    Boolean update = false;

    JFrame frame = new JFrame("ACTION GUI");

    GraphicsEnvironment ge = GraphicsEnvironment.getLocalGraphicsEnvironment();

    JButton move = new JButton("Move");

    JButton attack = new JButton("Attack");

    JButton drop = new JButton("Drop");

    JButton reload = new JButton("Relaod");

    JButton turnNorth = new JButton("▲");

    JButton turnSouth = new JButton("▼");

    JButton turnEast = new JButton("►");

    JButton turnWest = new JButton("◄");

    JButton wepOne = new JButton("GET WEP1");

    JButton wepTwo = new JButton("GET WEP2");

    JLabel label = new JLabel("Ammo: NONE");

    JPanel p2 = new JPanel();

    /**Constructor for the invoker... displays the actions*/
    public Invoker()
    {
        JPanel p1 = new JPanel();
        JPanel p3 = new JPanel();
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setSize(300, 200);
        frame.setLocationRelativeTo(null);

        /**Focus on frame*/
        frame.addKeyListener(this);
        frame.setFocusableWindowState(true);
        frame.setFocusable(true);
        p1.setFocusable(false);
        p2.setFocusable(false);
        p3.setFocusable(false);
        move.setFocusable(false);
        attack.setFocusable(false);
        drop.setFocusable(false);
        reload.setFocusable(false);
        turnNorth.setFocusable(false);
        turnSouth.setFocusable(false);
        turnEast.setFocusable(false);
        turnWest.setFocusable(false);
        wepOne.setFocusable(false);
        wepTwo.setFocusable(false);

        /**Panel 1*/
        p1.setLayout(new BorderLayout());
        p1.add(turnNorth, BorderLayout.NORTH);
        p1.add(move, BorderLayout.CENTER);
        p1.add(turnSouth, BorderLayout.SOUTH);
        p1.add(turnEast, BorderLayout.EAST);
        p1.add(turnWest, BorderLayout.WEST);
        /**Panel 2*/
        p2.setLayout(new GridLayout(2, 2));
        p2.add(attack);
        p2.add(drop);
        p2.add(reload);
        p2.add(label);
        p3.add(wepOne, new GridLayout(2, 1));
        p3.add(wepTwo);

        /** Add Panels to the JFrame*/
        frame.add(p1, BorderLayout.NORTH);
        frame.add(p3, BorderLayout.CENTER);
        frame.add(p2, BorderLayout.SOUTH);

        /** Display*/
        int i = (int) ge.getMaximumWindowBounds().getWidth();
        frame.setLocation(i - frame.getWidth() - 200, 100);
        frame.setResizable(false);
        frame.pack();
        frame.setVisible(true);

        getPressed();

    }

    /**Adding command to the Arraylist
     * @param the command to be entered
     * */
    public void addCommand(Command c)
    {
        comQ.add(c);

    }

    /**Acquire Command*/
    public void performAcquire()
    {

        comQ.get(acI).performCommand();
        stateChanged();
    }

    /**
     * perform an attack (Not working)
     */
    public void performAttack()
    {
        comQ.get(atI).performCommand();
        stateChanged();
    }

    /**
     * perform Drop(Not working)
     */
    public void performDrop()
    {

        comQ.get(drI).performCommand();
        stateChanged();

    }

    /**
     * performMove
     */
    public void performMove()
    {

        comQ.get(moI).performCommand();
        stateChanged();
    }

    /**
     * perform Reload Command
     */
    public void performReload()
    {
        comQ.get(reI).performCommand();
        stateChanged();
    }

    /**
     * perform turn command
     */
    public void performTurn()
    {
        comQ.get(tuI).performCommand();
        stateChanged();
    }

    /**
     * Updates the map
     */
    public void stateChanged()
    {
        update = true;
        observer.updateMap(update);
        update = false;
    }

    /**Gets when one of the jbuttons is pressed*/

    public void getType()
    {
        int i = 0;
        for (Command q : comQ)
        {
            if (q instanceof Acquire)
                acI = i;
            else if (q instanceof Attack)
                atI = i;
            else if (q instanceof Drop)
                drI = i;
            else if (q instanceof Move)
                moI = i;
            else if (q instanceof Reload)
                reI = i;
            else if (q instanceof Turn)
                tuI = i;

            i++;
        }
    }

    /**
     * Get When Somehting Is Press... also prints the ammo
     */
    private void getPressed()
    {

        attack.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg1)
            {
                if (lf != null && lf.getWeapon() != null)
                {
                    label.setText("Ammo:" + lf.getWeapon().getCurrentAmmo());
                    stateChanged();
                }
                lf.getWeapon().newRound(); //for now a round is whenever it fires
                performAttack();
            }
        });
        drop.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg2)
            {

                label.setText("Ammo: NONE");
                stateChanged();
                performDrop();
            }
        });
        move.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg3)
            {
                if (lf != null && lf.getWeapon() != null)
                {
                    label.setText("Ammo:" + lf.getWeapon().getCurrentAmmo());
                    stateChanged();
                }
                performMove();
            }
        });
        reload.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg4)
            {
                if (lf != null && lf.getWeapon() != null)
                {
                    label.setText("Ammo:" + lf.getWeapon().getCurrentAmmo());
                    stateChanged();
                }
                performReload();
            }
        });
        turnNorth.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg5)
            {
                lf.turn("West");
                performTurn();
            }
        });
        turnSouth.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg6)
            {
                lf.turn("East");
                performTurn();
            }
        });
        turnEast.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg7)
            {
                lf.turn("South");
                performTurn();
            }
        });
        turnWest.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg8)
            {
                lf.turn("North");
                performTurn();
            }
        });
        wepOne.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg8)
            {
                if (lf != null && lf.getWeapon() != null)
                {
                    label.setText("Ammo:" + lf.getWeapon().getCurrentAmmo());
                    stateChanged();
                }
                lf.wepWanted = 1;
                performAcquire();
            }
        });
        wepTwo.addActionListener(new ActionListener()
        {
            @Override
            public void actionPerformed(ActionEvent arg8)
            {
                if (lf != null && lf.getWeapon() != null)
                {
                    label.setText("Ammo:" + lf.getWeapon().getCurrentAmmo());
                    stateChanged();
                }
                lf.wepWanted = 2;
                performAcquire();
            }
        });
    }

    @Override
    public void keyPressed(KeyEvent e)
    {
        int key = e.getKeyCode();
        if (key == KeyEvent.VK_UP)
        {
            if (lf.getCurrentDirection().equals("NORTH"))
                performMove();
            else
            {
                lf.turn("NORTH");
                performTurn();
            }
        }
        if (key == KeyEvent.VK_DOWN)
        {
            if (lf.getCurrentDirection().equals("SOUTH"))
                performMove();
            else
            {
                lf.turn("SOUTH");
                performTurn();
            }
        }
        if (key == KeyEvent.VK_LEFT)
        {
            if (lf.getCurrentDirection().equals("WEST"))
                performMove();
            else
            {
                lf.turn("WEST");
                performTurn();
            }
        }
        if (key == KeyEvent.VK_RIGHT)
        {
            if (lf.getCurrentDirection().equals("EAST"))
                performMove();
            else
            {
                lf.turn("EAST");
                performTurn();
            }

        }
    }

    @Override
    public void keyReleased(KeyEvent arg0)
    {
        // TODO Auto-generated method stub

    }

    @Override
    public void keyTyped(KeyEvent arg0)
    {
        // TODO Auto-generated method stub

    }

}
