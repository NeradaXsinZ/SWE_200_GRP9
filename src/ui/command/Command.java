package ui.command;

public interface Command
{
    public void performCommand();
}
