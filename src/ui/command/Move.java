package ui.command;

import environment.Environment;
import lifeform.LifeForm;

public class Move implements Command
{
    LifeForm lf;

    Environment env = Environment.getWorldInstance();

    public Move(LifeForm lf)
    {
        this.lf = lf;
    }

    @Override
    public void performCommand()
    {
        env.move(lf);
    }

    public void setSpeed(int speed)
    {
        lf.setMaxSpeed(speed);
    }

}
