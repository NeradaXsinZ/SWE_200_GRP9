//RYAN HANDLEY
package ui.command;

import lifeform.LifeForm;

public class Turn implements Command
{
    private LifeForm lf;

    private String dir;

    public Turn(LifeForm lf)
    {
        this.lf = lf;

    }

    @Override
    public void performCommand()
    {
        if (lf != null)
            dir = lf.getCurrentDirection();
        lf.turn(dir);
    }

    /**
     * sets the direction of the way we want the lifeform to face
     * @param st
     */
    public void setDirection(String st)
    {
        st = st.toUpperCase();

        if (st.equals("NORTH") || st.equals("SOUTH") || st.equals("EAST") || st.equals("WEST"))
        {
            dir = st;
        }
        else
        {
            System.out.print(st + "=INVALID COMMAND");
        }

    }

}
