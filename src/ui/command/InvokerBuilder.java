/**@author Reginald Nettey*/
package ui.command;

import java.util.ArrayList;

import lifeform.LifeForm;
import environment.Environment;

public class InvokerBuilder
{
    Invoker inv;;

    private LifeForm lf;

    Environment env = Environment.getWorldInstance();

    private Acquire acq;

    private Attack att;

    private Drop drop;

    private Move move;

    private Reload rel;

    private Turn turn;

    public InvokerObserver observer;

    public void addObserver(InvokerObserver observer)
    {
        inv.observer = observer;
    }

    /**
     * Constructor for invokerBuilder Creates 
     * An invoker and sets commands
     * @param invoker the invoker being made 
     * @Param lf the lifeform to be added to the commands
     */
    public InvokerBuilder(Invoker invoker, LifeForm lf)
    {
        invoker = new Invoker();
        inv = invoker;
        this.lf = lf;
        acq = new Acquire(lf);
        att = new Attack(lf);
        drop = new Drop(lf);
        move = new Move(lf);
        move.setSpeed(1);
        rel = new Reload(lf);
        turn = new Turn(lf);
        inv.lf = lf;
        inv.addCommand(acq);
        inv.addCommand(att);
        inv.addCommand(drop);
        inv.addCommand(move);
        inv.addCommand(rel);
        inv.addCommand(turn);
        inv.getType();
    }

}
