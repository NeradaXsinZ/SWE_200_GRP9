package ui.command;

import weapon.Weapon;
import lifeform.LifeForm;

public class Reload implements Command
{
    LifeForm lf;

    Weapon wep;

    public Reload(LifeForm lf)
    {
        this.lf = lf;
        if (lf.getWeapon() != null)
            wep = lf.getWeapon();
    }

    /**
     * if the lifeform is carrying a weapon, this will reload the weapon
     */
    @Override
    public void performCommand()
    {
        if (lf.getWeapon() != null)
        {
            lf.getWeapon().reload();
            ;
        }

    }

}
