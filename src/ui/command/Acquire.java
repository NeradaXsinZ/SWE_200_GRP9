/**@Author Regi Nettey*/
package ui.command;

import weapon.Weapon;
import environment.Environment;
import lifeform.LifeForm;

public class Acquire implements Command
{

    Weapon wep;

    LifeForm lf;

    Environment env = Environment.getWorldInstance();

    int loc, x, y;

    public Acquire(LifeForm lf)
    {
        this.lf = lf;
        x = lf.getXLoc();
        y = lf.getYLoc();
    }

    @Override
    public void performCommand()
    {
        loc = lf.wepWanted;
        findWeapon(loc);
        x = lf.getXLoc();
        y = lf.getYLoc();
        try
        {
            if (lf.getWeapon() == null)
            {
                lf.addWeapon(wep);
                env.getCellArr()[lf.getXLoc()][lf.getYLoc()].removeWeapon(env.getCellArr()[x][y].getWeaponInCell(loc));

            }
        }
        catch (NullPointerException e)
        {
            System.out.println("Sometheing was Null");
        }
    }

    public void findWeapon(int loc)
    {
        loc -= 1;
        lf.wepWanted = loc;

        this.loc = loc;
        x = lf.getXLoc();
        y = lf.getYLoc();
        if (loc >= 0 && loc <= 1 && lf.getWeapon() == null && env != null)
        {
            try
            {
                wep = env.getCellArr()[x][y].getWeaponInCell(loc);
            }
            catch (NullPointerException e)
            {
                System.out.println("Can't find wepon");
            }
        }
    }
}
