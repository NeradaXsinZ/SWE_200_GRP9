//RYAN HANDLEY
package ui.command;

import java.util.Vector;

import weapon.Weapon;
import environment.Cell;
import environment.Environment;
import lifeform.LifeForm;

public class Drop implements Command
{
    Environment env;

    LifeForm lf;

    /**
     * constructor
     */
    public Drop(LifeForm lf)
    {
        this.lf = lf;
        env = Environment.getWorldInstance();
    }

    /**
     * if the lifeform is carrying a weapon, the lifeForm will drop it, and it 
     * will be inserted into the cell where it resides if there is room
     */
    @Override
    public void performCommand()
    {
        if ((env.getCellArr()[lf.getXLoc()][lf.getYLoc()].getNumWeapons() < 2) && lf.getWeapon() != null)
        {
            env.addWeaponToEnvironment(lf.getXLoc(), lf.getYLoc(), lf.getWeapon());
            lf.dropWeapon();
        }

    }

}
