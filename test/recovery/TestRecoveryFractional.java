/*
 * Programmer:Reginald Nettey
 * Professor:Dr. Girard 
 * Course:Design Patterns
 * Description:Junit tests for fractional recovery
 */
package recovery;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestRecoveryFractional
{
    RecoveryFractional frac;

    int maxHp, result;

    /**
     * 
     * Set up what to do before every test
     */
    @Before
    public void setup()
    {
        frac = new RecoveryFractional(.1);
        maxHp = 62;
    }

    /**.
     * Test to see that amount recovered is rounded up
     * Ceiling of (.1*62)
     */
    @Test
    public void testRoundsUpWhenHurt()
    {
        result = frac.calculateRecovery(23, maxHp);
        assertEquals(30, result);
    }

    /**
     * Test to check return of calc. recovery when no harm is done 
     */
    @Test
    public void testWhenNotHurt()
    {
        result = frac.calculateRecovery(maxHp, maxHp);
        assertEquals(maxHp, result);
    }

    /**
     * Test to check When just below full health
     */
    @Test
    public void testWhenHurtALittle()
    {
        result = frac.calculateRecovery(60, maxHp);
        assertEquals(maxHp, result);
    }

    /**
     * Tests what happens with recovery when very injured
     */
    @Test
    public void testWhenVeryHurt()
    {
        result = frac.calculateRecovery(5, maxHp);
        assertEquals(12, result);
    }

    /**
     * Test When recovery recovers exact amount to get to max hit points
     */
    @Test
    public void testPerfectRecovery()
    {
        result = frac.calculateRecovery(55, maxHp);
        assertEquals(maxHp, result);
    }

    /**
     * Test to see that hit points don't recover when at 0 or "dead"
     */
    @Test
    public void testWhenDead()
    {
        result = frac.calculateRecovery(0, maxHp);
        assertEquals(0, result);
    }
}
