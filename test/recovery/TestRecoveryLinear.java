/*
 * Programmer:Reginald Nettey
 * Professor:Dr. Girard 
 * Course:Design Patterns
 * Description:Tests for linear(set) recovery 
 */
package recovery;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestRecoveryLinear
{
    RecoveryLinear r1;

    int maxHp, result;

    /**
     * Setup initial conditions before each test
     */
    @Before
    public void setup()
    {
        r1 = new RecoveryLinear(25);
        maxHp = 50;
    }

    /**
     * Make sure nothing happens when Hp is full
     */
    @Test
    public void noRevoveryWhenNotHurt()
    {
        result = r1.calculateRecovery(maxHp, maxHp);
        assertEquals(maxHp, result);
    }

    /**
     * Test to make sure correct amount is healed when really hurt
     */
    @Test
    public void testRecoveryWhenReallyHurt()
    {
        result = r1.calculateRecovery(5, maxHp);
        assertEquals(30, result);
    }

    /**
     * Test to make sure it doesn't heal past maxHp when only a little hurt
     */
    @Test
    public void testWhenHurtOnlyHurtsALittle()
    {
        result = r1.calculateRecovery(45, maxHp);
        assertEquals(maxHp, result);
    }

    /**
     * Test when amount healed is just enough to get to max hp
     */
    @Test
    public void testPerfectRecovery()
    {
        result = r1.calculateRecovery(25, maxHp);
        assertEquals(maxHp, result);
    }

    /**
     * Test to make sure hp stays at 0 if dead and tries to recover
     */
    @Test
    public void testWhenDead()
    {
        result = r1.calculateRecovery(0, maxHp);
        assertEquals(0, result);
    }

}
