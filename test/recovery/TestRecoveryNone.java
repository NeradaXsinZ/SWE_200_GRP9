/*
 * Programmer:Reginald Nettey
 * Professor:Dr. Girard 
 * Course:Design Patterns
 * Description: Tests for when there is no recovery
 */
package recovery;

import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.Test;

public class TestRecoveryNone
{
    RecoveryBehavior none;

    int maxHitPt;

    /**
     * Setup initial conditions before each test
     */
    @Before
    public void setup()
    {
        maxHitPt = 40;
        none = new RecoveryNone();

    }

    /**
     * Test that nothing happens when Hp is full
     */
    @Test
    public void testRecoveryNoneWhenNotHurt()
    {
        int result = none.calculateRecovery(maxHitPt, maxHitPt);
        assertEquals(maxHitPt, result);
    }

    /**
     * Test That nothing happens when Hp is not full
     */
    @Test
    public void testRecoveryNoneWhenHurt()
    {
        int result = none.calculateRecovery(20, maxHitPt);
        assertEquals(20, result);
    }

}
