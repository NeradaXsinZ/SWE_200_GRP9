package environment;

import static org.junit.Assert.*;

import javax.swing.JOptionPane;

import lifeform.Alien;
import lifeform.Human;
import recovery.RecoveryLinear;
import weapon.ChainGun;
import weapon.Pistol;
import weapon.PlasmaCannon;

import org.junit.Test;

/**
 * Test the map
 * @author Kyle Jones and Tim Haus
 *
 */
public class TestMap
{
    RecoveryLinear linear;

    /**
     * TODO Kyle Jones
     * tests the display of a human
     */
    @Test
    public void testHuman()
    {

        Environment.makeEnvironment(4, 4);
        Environment environment = Environment.getWorldInstance();

        Human bob = new Human("Bob", 200, 15);

        environment.addLifeFormToEnvironment(2, 2, bob); //add human to 2,2
        Map map = new Map(environment); //create map of environment

        //human is displayed in spot 2,2
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there " + "a human\n being displayed in cell (2,2)"));

        environment.destroyEnvironment();
    }

    /**
     * TODO Kyle Jones
     * Tests the display of an Alien
     */
    @Test
    public void testAlien()
    {

        Environment.makeEnvironment(4, 4);
        Environment environment = Environment.getWorldInstance();
        Alien steve = new Alien("Steve", 60, linear, 2);

        environment.addLifeFormToEnvironment(3, 2, steve); //add alien to 3,2

        Map map = new Map(environment); //create map

        //test Alien is being displayed in 3,2
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there " + "an Alien\n being displayed in cell (3,2)"));

        environment.destroyEnvironment();

    }

    /**
     * TODO Kyle Jones
     * test the display of a gun that a lifeform is holding
     */
    @Test
    public void testLifeFormWithGun()
    {
        Environment.makeEnvironment(4, 4);
        Environment environment = Environment.getWorldInstance();
        Human bob = new Human("Bob", 200, 15);
        Human toby = new Human("Toby", 200, 15);
        Alien steve = new Alien("Steve", 60, linear, 2);

        ChainGun cg = new ChainGun();
        bob.addWeapon(cg); //bob gets a ChainGun

        Pistol p = new Pistol();
        toby.addWeapon(p); //toby gets a Pistol

        PlasmaCannon pc = new PlasmaCannon();
        steve.addWeapon(pc); //steve gets a PlasmaCannon

        environment.addLifeFormToEnvironment(2, 2, bob); //bob goes to cell 2,2
        environment.addLifeFormToEnvironment(1, 2, steve); //steve goes to cell 1,2
        environment.addLifeFormToEnvironment(2, 1, toby); //toby goes to cell 2,1
        Map map = new Map(environment);

        //Makes sure a human with a chain gun displays
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there " + "a human\n with a chain gun being displayed\n in cell (2,2)"));
        //Makes sure a alien with a plasma cannon displays
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there " + "a alien\n with a plasma cannon being displayed\n in cell (1,2)"));
        //Makes sure a human with a chain gun displays
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there " + "a human\n with a chain gun being displayed\n in cell (2,1)"));

        environment.destroyEnvironment();

    }

    /**
     * TODO Kyle Jones
     * Test displaying a gun in each cell
     */
    @Test
    public void testGunInCell()
    {
        Environment.makeEnvironment(4, 4);
        Environment environment = Environment.getWorldInstance();

        ChainGun cg = new ChainGun();
        Pistol p = new Pistol();
        PlasmaCannon pc = new PlasmaCannon();

        assertTrue(environment.addWeaponToEnvironment(1, 1, cg)); //add weapon to spot1
        assertTrue(environment.addWeaponToEnvironment(1, 1, p)); //add weapon to spot2
        assertTrue(environment.addWeaponToEnvironment(0, 1, pc)); //add weapon to spot1

        Map map = new Map(environment); //creates map

        //test that guns display correctly in spot1
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there " + "a chain gun\n being displayed in cell (1,1) position 1"));
        //test that guns display correctly in spot2
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there " + "a pistol\n being displayed in cell (1,1) position 2"));
        //test that the final type of gun(plasma cannon) also displays
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there " + "a plasma cannon\n being displayed in cell (0,1) position 1"));

        environment.destroyEnvironment();

    }

    /**
     * TODO Kyle Jones
     *	tests passing the environment back into update map after something changed 
     */
    @Test
    public void testUpdate()
    {
        Environment.makeEnvironment(4, 4);
        Environment environment = Environment.getWorldInstance();

        Map map = new Map(environment); //create map
        //make sure map is empty
        // assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is the map empty?"));

        ChainGun cg = new ChainGun();
        Pistol p = new Pistol();

        assertTrue(environment.addWeaponToEnvironment(1, 1, cg)); //add cg to cell 1,1 spot1
        assertTrue(environment.addWeaponToEnvironment(1, 1, p)); //add p to cell 1,1 spot2

        map.updateMap(environment); //update map with environment
        //make sure the ChainGun is displaying in spot1
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there a chain gun in cell (1,1) position 1"));
        //make sure the is Pistol displaying in spot2
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there a pistol in cell (1,1) position 2"));

        environment.destroyEnvironment();

    }

}
