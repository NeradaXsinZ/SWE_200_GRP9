package environment;

import static org.junit.Assert.*;

import javax.swing.JOptionPane;

import org.junit.Test;

/**
 * Test the set up of the Legend
 * @author Kyle Jones and Tim Haus
 *
 */
public class TestLegend
{
    /**
     * Test the legend
     */
    @Test
    public void testLegend()
    {
        Environment.makeEnvironment(4, 4);
        Environment environment = Environment.getWorldInstance();

        Map map = new Map(environment); //create map
        //is the human circle displaying
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there a green circle for humans?"));
        //is the alien square displaying
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there a orange square for aliens?"));
        //is the pink pistol gun displaying
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there a pink circle for pistol?"));
        //is the yellow chain gun displaying
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there a yellow circle for chain gun?"));
        //is the white plasma cannon displaying
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there a white circle for plasma cannon?"));

    }

}
