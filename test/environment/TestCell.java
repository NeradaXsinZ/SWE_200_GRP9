/*

 * Programmer:Reginald Nettey
 * Professor:Dr. Girard 
 * Course:Design Patterns
 * Description:The test cases for the Cell class
 */
package environment;

import static org.junit.Assert.*;
import lifeform.*;

import org.junit.Before;
import org.junit.Test;

import weapon.*;

public class TestCell
{

    Cell cell;

    LifeForm bob, fred, mike;

    /**
    * creates instances of MockLifeForm and cell before each test
    */
    @Before
    public void setup()
    {
        bob = new MockLifeForm("Robert", 40);
        fred = new MockLifeForm("Fred", 40);
        mike = new MockLifeForm("Mike", 40);
        cell = new Cell();
    }

    @Test
    public void testInitaializationWithWeapon()
    {
        Cell cellNew = new Cell();
        assertNull(cellNew.getLifeForm());
        assertEquals(0, cellNew.getNumWeapons());
    }

    @Test
    public void testAddWeapons()
    {

        assertTrue(cell.addWeapon(new Pistol()));
        assertTrue(cell.addWeapon(new ChainGun()));
        assertFalse(cell.addWeapon(new PlasmaCannon()));
        assertEquals(2, cell.getNumWeapons());
    }

    @Test
    public void testRemoveWeapons()
    {
        Weapon pistol = new Pistol();
        Weapon chain = new ChainGun();
        cell.addWeapon(pistol);
        assertEquals(1, cell.getNumWeapons());
        cell.addWeapon(chain);
        assertEquals(2, cell.getNumWeapons());
        cell.removeWeapon(pistol);
        assertEquals(1, cell.getNumWeapons());

    }

    /**Decorator Tests Begin Here*/

    /**
     * At initialization the Cell should be empty and not contain a LifeForm
     */
    @Test
    public void testInitialization()
    {
        Cell cellNew = new Cell();
        assertNull(cellNew.getLifeForm());
    }

    /**
     * Test creating LifeForms
     */
    @Test
    public void testSetLifeForm()
    {

        assertTrue(cell.addLifeForm(bob));
        assertEquals(bob, cell.getLifeForm());
        assertFalse(cell.addLifeForm(fred));
        assertEquals(bob, cell.getLifeForm());
    }

    /**
     * Test remove and re-add LifeForm to the same cell 	
     */
    @Test
    public void testRemoveAndReAddLifeForm()
    {
        cell.addLifeForm(bob);
        assertNotNull(cell.getLifeForm());
        assertEquals(cell.getLifeForm().getName(), "Robert");
        cell.removeLifeForm();
        assertNull(cell.getLifeForm());
        cell.addLifeForm(mike);
        assertNotNull(cell.getLifeForm());
        assertEquals(cell.getLifeForm().getName(), "Mike");

    }

}
