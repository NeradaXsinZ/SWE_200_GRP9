/*
 * Programmer:Reginald Nettey
 * Professor:Dr. Girard 
 * Course:Design Patterns
 * Description:Junit test for environment class
 */
package environment;

import static org.junit.Assert.*;
import lifeform.*;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import weapon.*;

public class TestEnvironment
{
    /**
     * Declare variables
     */
    LifeForm jaylin, pete, bob;

    Environment room;

    /**
     * Create instances of room, and the 3 life forms that are used throughout
     * the tests before any of the tests are run
     */
    @Before
    public void setup()
    {

        Environment.makeEnvironment(2, 3);
        jaylin = new MockLifeForm("Jaylin", 20);
        pete = new MockLifeForm("Pete", 30);
        bob = new MockLifeForm("Robert", 9001, 1);
        room = Environment.getWorldInstance();

    }

    @After
    public void complete()
    {
        room.destroyEnvironment();
    }

    /**
     * Tests that a Human or Alien can move properly
     */
    @Test
    public void testMovementWithAliensAndLifeForms()
    {
        Human Linus = new Human("Linus", 50, 50);
        Alien Xenu = new Alien("Xenu", 50, null, 10);

        room.destroyEnvironment();
        Environment.makeEnvironment(20, 20);
        room = Environment.getWorldInstance();

        room.addLifeFormToEnvironment(0, 0, Linus);
        Linus.turn("South");
        room.move(Linus);
        assertEquals("Linus", room.getCellArr()[0][3].getLifeForm().getName());
        // Test that a Human can move South without obstacles
        room.addLifeFormToEnvironment(0, 5, Xenu);
        room.move(Linus);
        assertEquals("Linus", room.getCellArr()[0][4].getLifeForm().getName());
        // Test that a Human can move South with obstacles

        room.removeLifeFormFromEnvironment(0, 5);
        room.removeLifeFormFromEnvironment(0, 4);

        room.addLifeFormToEnvironment(5, 5, Xenu);
        room.addLifeFormToEnvironment(5, 10, Linus);
        Linus.turn("NORTH");
        room.move(Linus);
        assertEquals("Linus", room.getCellArr()[5][7].getLifeForm().getName());
        // Test that a human can move North without obstacles
        room.move(Linus);
        assertEquals("Linus", room.getCellArr()[5][6].getLifeForm().getName());
        // Test that a human can move North with obstacles

        room.removeLifeFormFromEnvironment(5, 7);
        room.removeLifeFormFromEnvironment(5, 6);

        room.addLifeFormToEnvironment(9, 8, Linus);
        room.addLifeFormToEnvironment(5, 8, Xenu);
        Xenu.turn("EAST");
        room.move(Xenu);
        Xenu.setMaxSpeed(2);
        assertEquals("Xenu", room.getCellArr()[7][8].getLifeForm().getName());
        // Tests that an Alien can move East without obstacles
        room.move(Xenu);
        assertEquals("Xenu", room.getCellArr()[8][8].getLifeForm().getName());
        // Test that an Alien can move West with obstacles

        Xenu.turn("WEST");
        room.removeLifeFormFromEnvironment(9, 8);
        room.addLifeFormToEnvironment(3, 7, Linus);
        room.move(Xenu);
        assertEquals("Xenu", room.getCellArr()[6][8].getLifeForm().getName());
        // Test that an ALien can move East without obstacles
        room.move(Xenu);
        assertEquals("Xenu", room.getCellArr()[4][8].getLifeForm().getName());
        // Test that an Alien can move East with obstacles
    }

    /**
     * Test that a LifeForm can move properly with obstacles in its way
     */
    @Test
    public void testMoveWithObstacles()
    {
        room.addLifeFormToEnvironment(1, 1, bob);
        room.addLifeFormToEnvironment(1, 2, pete);
        pete.setMaxSpeed(1);
        room.move(pete);
        assertEquals("Robert", room.getCellArr()[1][1].getLifeForm().getName());
        assertEquals("Pete", room.getCellArr()[1][2].getLifeForm().getName());
        // Shows that a lifeform can't move to a cell that already contains a
        // lifeform

        room.destroyEnvironment();
        Environment.makeEnvironment(5, 5);
        room = Environment.getWorldInstance();
        room.addLifeFormToEnvironment(1, 1, jaylin);
        room.addLifeFormToEnvironment(1, 3, bob);
        bob.setMaxSpeed(3);
        room.move(bob);
        assertEquals("Robert", room.getCellArr()[1][2].getLifeForm().getName());
        assertNull(room.getCellArr()[1][3].getLifeForm());
        jaylin.setMaxSpeed(3);
        room.move(jaylin);
        assertEquals("Jaylin", room.getCellArr()[1][0].getLifeForm().getName());
        // Ensures that a lifeform will not walk off the map
        
    }
    
   

    /**
     * Test that a lifeform can move without obstacles in its way
     */
    @Test
    public void testMoveWithoutObstacles()
    {
        
        
        room.addLifeFormToEnvironment(0, 1, bob);

        bob.setMaxSpeed(1);
        room.move(bob);
        assertEquals(1, bob.maxSpeed);
        assertEquals("Robert", room.getCellArr()[0][0].getLifeForm().getName());
        // Test a lifeform can move North
        assertNull(room.getCellArr()[0][1].getLifeForm());
        // Ensures we don't have two bobs
        room.move(bob);
        assertEquals("Robert", room.getCellArr()[0][0].getLifeForm().getName());
        // Test a lifeform won't move off an edge (North)

        bob.turn("SOUTH");
        room.move(bob);
        assertEquals("Robert", room.getCellArr()[0][1].getLifeForm().getName());
        // Test a lifeform can move South

        bob.turn("EAST");
        room.move(bob);
        assertEquals("Robert", room.getCellArr()[1][1].getLifeForm().getName());
        // Test a lifeform can move East

        bob.turn("WEST");
        room.move(bob);
        assertEquals("Robert", room.getCellArr()[0][1].getLifeForm().getName());
        // Test a lifeform can move West

    }

    /**
     * Test getting a life form type
     */
    @Test
    public void testGetLifeFormType()
    {
        Environment.makeEnvironment(4, 4);
        Environment environment = Environment.getWorldInstance();

        assertEquals(" ", environment.getLifeFormType(0, 0)); //no lifeform
        Human bob = new Human("Bob", 40, 10);
        Alien steve = new Alien("Steve", 400, null, 10);

        environment.addLifeFormToEnvironment(1, 1, bob); //human in 1,1
        environment.addLifeFormToEnvironment(0, 2, steve); //alien in 0,2

        assertEquals("Human", environment.getLifeFormType(1, 1)); //testing human
        assertEquals("Alien", environment.getLifeFormType(0, 2)); //testing alien

        environment.destroyEnvironment();

    }

    /**
     * test getting the gun type
     */
    @Test
    public void testGetGunType()
    {
        Environment.makeEnvironment(4, 4);
        Environment environment = Environment.getWorldInstance();

        assertEquals(" ", environment.getGunType(0, 0, 0)); //test when theres no gun
        ChainGun cg = new ChainGun();
        Pistol p = new Pistol();

        assertTrue(environment.addWeaponToEnvironment(0, 0, cg));
        assertTrue(environment.addWeaponToEnvironment(0, 0, p));

        assertEquals("Pistol", environment.getGunType(0, 0, 1)); //test when there is a chain gun in spot 2
        assertEquals("Chain Gun", environment.getGunType(0, 0, 0)); //test when there is a chain gun in spot 1

        environment.destroyEnvironment();
    }

    /**
     * Command Pattern Tests start above here
     */
    @Test
    public void testAddWeaponToEnvironment()
    {
        assertTrue(room.getCellArr()[0][0] == null);
        assertTrue(room.addWeaponToEnvironment(1, 2, new Pistol()));
        assertEquals(1, room.getCellArr()[1][2].getNumWeapons());
        assertTrue(room.addWeaponToEnvironment(0, 0, new ChainGun()));
        room.destroyEnvironment();
    }

    @Test
    public void testRemoveWeaponFromEnvironment()
    {
        Weapon pistol = new Pistol();
        Weapon chain = new ChainGun();
        assertTrue(room.addWeaponToEnvironment(1, 2, pistol));
        assertTrue(room.addWeaponToEnvironment(1, 2, chain));
        assertEquals(2, room.getCellArr()[1][2].getNumWeapons());
        assertTrue(room.removeWeaponFromEnvironment(1, 2, pistol));
        assertEquals(1, room.getCellArr()[1][2].getNumWeapons());
        room.destroyEnvironment();
    }

    @Test
    public void testRange()
    {
        room.destroyEnvironment();
        Environment.makeEnvironment(4, 4);
        room = Environment.getWorldInstance();
        room.addLifeFormToEnvironment(0, 0, bob);
        room.addLifeFormToEnvironment(0, 2, jaylin);
        room.addLifeFormToEnvironment(3, 2, pete);
        // Same Row
        assertEquals(20, room.computeRange(bob, jaylin));
        // Same Column
        assertEquals(30, room.computeRange(jaylin, pete));
        // Different Row and Column
        assertEquals(36, room.computeRange(bob, pete));
    }

    /** Decorator tests */

    /**
     * Creates a new one by one environment P.S. I could not think of any other
     * way to test that it was only one by one so I added this try catch to try
     * to go one beyond the last index of cellArr
     */
    @Test
    public void testInitialization()
    {
        Environment.makeEnvironment(1, 1);
        ;
        assertTrue(room.getCellArr()[0][0] == null);
        try
        {
            assertTrue(room.getCellArr()[0][1] == null);
        }
        catch (IndexOutOfBoundsException e)
        {
            System.out.print("\n--- Cell Array Index Out Of Bounds---\n\n");
        }

    }

    /**
     * Adds 2 life forms to the room environment and prints their data to the
     * screen and checks for their names in the indexes in which they are added
     */
    @Test
    public void testAdd()
    {

        assertTrue(room.getCellArr()[0][0] == null);
        assertTrue(room.addLifeFormToEnvironment(1, 2, jaylin));
        assertEquals(room.getCellArr()[1][2].getLifeForm().getName(), "Jaylin");
        assertTrue(room.addLifeFormToEnvironment(0, 0, pete));
        assertEquals(room.getCellArr()[0][0].getLifeForm().getName(), "Pete");

    }

    /**
     * Print out a message with the name of the failed person
     */
    @Test
    public void testAddOutOfBounds()
    {
        assertFalse(room.addLifeFormToEnvironment(2, 2, bob));

    }

    /**
     * Test the remove method by adding an Instance of bob and one of jack and
     * confirming it's there then removing said instance and confirming that it
     * is gone while making sure that jack is still there
     */
    @Test
    public void testRemove()
    {

        assertTrue(room.addLifeFormToEnvironment(0, 0, bob));
        assertTrue(room.addLifeFormToEnvironment(0, 1, jaylin));
        assertEquals(room.getCellArr()[0][0].getLifeForm().getName(), "Robert");
        assertEquals(room.getCellArr()[0][1].getLifeForm().getName(), "Jaylin");
        assertTrue(room.removeLifeFormFromEnvironment(0, 0));
        assertEquals(room.getCellArr()[0][1].getLifeForm().getName(), "Jaylin");
        assertTrue(room.getCellArr()[0][0].getLifeForm() == null);

    }

    /**
     * Should return an exception prints
     * "Index out of bounds: Space does not exist"
     */
    @Test
    public void testRemoveOutOfBounds()
    {

        assertFalse(room.removeLifeFormFromEnvironment(0, 3));

    }

}
