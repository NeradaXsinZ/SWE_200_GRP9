///*
// * Programmer:Reginald Nettey
// * Professor:Dr. Girard 
// * Course:Design Patterns
// * Description:A class that runs all test
// */
//import org.junit.runner.RunWith;
//import org.junit.runners.Suite;
//
//import recovery.*;
//import state.TestAIContext;
//import state.TestOutOfAmmo;
//import ui.command.TestInvokerBuilder;
//import weapon.TestChainGun;
//import weapon.TestPistol;
//import weapon.TestPlasmaCannon;
//import weapon.TestPowerBooster;
//import weapon.TestScope;
//import weapon.TestStabilizer;
//import environment.*;
//import lifeform.*;
//import gameplay.*;
//
///**
// * Runs all or the tests in this project
// *
// */
//@RunWith(Suite.class)
//@Suite.SuiteClasses({ TestLifeForm.class, TestCell.class, TestEnvironment.class, TestRecoveryFractional.class, TestRecoveryLinear.class, TestRecoveryNone.class, TestAlien.class, TestHuman.class,
//        TestSimpleTimer.class, TestPistol.class, TestStabilizer.class, TestPlasmaCannon.class, TestScope.class, TestPowerBooster.class, TestChainGun.class, TestMap.class, TestLegend.class,
//        TestInvokerBuilder.class, TestAIContext.class, TestOutOfAmmo.class})
//public class AllGameTests
//{
//
//}
