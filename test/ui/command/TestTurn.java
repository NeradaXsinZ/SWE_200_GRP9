

package ui.command;

import static org.junit.Assert.*;
import lifeform.Human;
import lifeform.LifeForm;

import org.junit.Before;
import org.junit.Test;

import environment.Environment;

public class TestTurn
{

    Environment env;

    LifeForm bob;

    @Before
    public void setup()
    {
        Environment.makeEnvironment(5, 5);
        env = Environment.getWorldInstance();
        bob = new Human("Bob", 100, 50);
        env.addLifeFormToEnvironment(1, 1, bob);
    }

    /**
     * Tests to make sure that when a lifeForm is initialized, it is facing in the North position
     */
    @Test
    public void testPlayerDirectionAtInitialization()
    {
        assertEquals("NORTH", bob.getCurrentDirection());
    }

    /**
     * Tests to make sure that we can make a lifeForm face to the East
     */
    @Test
    public void testEast()
    {
        //we should make sure bob is facing a direction other than east first
        assertEquals("NORTH", bob.getCurrentDirection());

        Turn cmd = new Turn(bob);
        cmd.setDirection("east");
        cmd.performCommand();
        assertEquals("EAST", bob.getCurrentDirection());
    }

    /**
     * tests to make sure we can make a lifeForm turn to the south
     */
    @Test
    public void testSouth()
    {
        Turn cmd = new Turn(bob);
        cmd.setDirection("south");
        cmd.performCommand();
        assertEquals("SOUTH", bob.getCurrentDirection());
    }

    /**
     * tests to make sure that we can make a lifeForm turn the the west, then back to the north
     */
    @Test
    public void testWestThenNorth()
    {
        Turn cmd = new Turn(bob);
        cmd.setDirection("west");
        cmd.performCommand();
        assertEquals("WEST", bob.getCurrentDirection());

        //now we'll make sure that our lifeForm can return to it's default direction, NORTH
        cmd.setDirection("north");
        cmd.performCommand();
        assertEquals("NORTH", bob.getCurrentDirection());
    }

}
