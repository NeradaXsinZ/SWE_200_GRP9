//RYAN HANDLEY
package ui.command;

import static org.junit.Assert.*;
import lifeform.Alien;
import lifeform.Human;
import lifeform.LifeForm;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import recovery.RecoveryNone;
import weapon.Pistol;
import environment.Environment;

public class testAttack
{

    Environment env;

    Pistol p = new Pistol();

    LifeForm bob;

    Alien zerg1;

    Alien zerg2;

    Alien zerg3;

    Alien zerg4;

    @Before
    public void setUp()
    {
        Environment.makeEnvironment(5, 5);
        env = Environment.getWorldInstance();
        bob = new Human("bob", 100, 50);
        bob.addWeapon(p);

        zerg1 = new Alien("zerg1", 100, new RecoveryNone(), 0);
        zerg2 = new Alien("zerg2", 100, new RecoveryNone(), 0);
        zerg3 = new Alien("zerg3", 100, new RecoveryNone(), 0);
        zerg4 = new Alien("zerg4", 100, new RecoveryNone(), 0);

        env.addLifeFormToEnvironment(2, 2, bob);
        env.addLifeFormToEnvironment(1, 2, zerg1);
        env.addLifeFormToEnvironment(2, 3, zerg2);
        env.addLifeFormToEnvironment(3, 2, zerg3);
        env.addLifeFormToEnvironment(2, 1, zerg4);
    }

    @After
    public void destroy()
    {
        env.destroyEnvironment();
    }

    /**
     * tests that a lifeForm can attack when it is facing North
     */
    @Test
    public void testAttackNorth()
    {
        //bob will attack the alien to the north of him with a pistol
        //his default direction is north, so we don't have to do anything
        Attack attack = new Attack(bob);
        attack.performCommand();
        assertEquals(90, zerg1.getCurrentLifePoints());
    }

    /**
     * tests that a lifeForm can attack when facing east
     */
    @Test
    public void testAttackEast()
    {
        bob.turn("EAST");
        Attack attack = new Attack(bob);
        attack.performCommand();
        assertEquals(90, zerg2.getCurrentLifePoints());
    }

    /**
     * tests that a lifeForm can attack when facing South
     */
    @Test
    public void testAttackSouth()
    {
        bob.turn("SOUTH");
        Attack attack = new Attack(bob);
        attack.performCommand();
        assertEquals(90, zerg3.getCurrentLifePoints());
    }

    /**
     * tests that a lifeform can ttack when facing west
     */
    @Test
    public void testAttackWest()
    {
        bob.turn("WEST");
        Attack attack = new Attack(bob);
        attack.performCommand();
        assertEquals(90, zerg4.getCurrentLifePoints());
    }

}
