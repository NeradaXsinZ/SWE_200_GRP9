package ui.command;

import static org.junit.Assert.*;
import lifeform.Alien;
import lifeform.Human;
import lifeform.LifeForm;

import org.junit.Before;
import org.junit.Test;

import recovery.RecoveryNone;
import environment.Environment;

public class TestMove
{
    Environment env;

    Human bob;

    Alien zerg;

    @Before
    public void setUp()
    {
        Environment.makeEnvironment(5, 5);
        env = Environment.getWorldInstance();
        bob = new Human("Bob", 100, 50);
        zerg = new Alien("zerg", 100, new RecoveryNone(), 0);
        env.addLifeFormToEnvironment(4, 4, bob);
        env.addLifeFormToEnvironment(4, 3, zerg);
    }

    /**
     * tests to make sure that a human will move in relation to its maxspeed. Humans have a 
     * maxspeed of three
     */
    @Test
    public void testHumanMoveCommand()
    {
        Move move = new Move(bob);
        //bob moves
        move.performCommand();
        //bob is still facing north, so he should move up three spaces
        assertEquals(4, bob.getXLoc());
        assertEquals(1, bob.getYLoc());

    }

}
