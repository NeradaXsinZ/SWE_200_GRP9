package ui.command;

import static org.junit.Assert.*;

import javax.swing.JOptionPane;

import lifeform.Alien;
import lifeform.Human;
import lifeform.LifeForm;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import recovery.RecoveryNone;
import weapon.ChainGun;
import weapon.Pistol;
import weapon.PlasmaCannon;
import weapon.Weapon;
import environment.Environment;
import environment.Map;

public class TestInvokerBuilder
{

    Environment room;

    LifeForm bob;

    Invoker inv;

    InvokerBuilder builder;

    Alien zork;

    public static final String NORTH = "NORTH", SOUTH = "SOUTH", EAST = "EAST", WEST = "WEST";

    @Before
    public void setup()
    {
        Environment.makeEnvironment(5, 5);
        room = Environment.getWorldInstance();
        inv = null;
        bob = new Human("Bob", 100, 50);
        zork = new Alien("Zork11", 50, new RecoveryNone(), 0);
        room.addLifeFormToEnvironment(2, 1, bob);
        room.addLifeFormToEnvironment(2, 3, zork);
    }

    @After
    public void finish()
    {
        room.destroyEnvironment();
    }

    @Test
    public void testFunctions()
    {
        Map window = new Map(room);
        PlasmaCannon c = new PlasmaCannon();
        room.addWeaponToEnvironment(3, 3, new ChainGun());
        bob.addWeapon(c);
        builder = new InvokerBuilder(inv, bob);
        builder.addObserver(window);

        try
        {
            Thread.sleep(20000); //20000 milliseconds 
        }
        catch (InterruptedException ex)
        {
            Thread.currentThread().interrupt();
        }
        /*These work, but the way the grid is dawn moving north does not move you up...    
         * I just went back to this... North will be West, and South will be East(i changed these in Invoker)
         * 
         */

        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Can you Turn North?"));
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Can you Turn East?"));
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Can you Turn South?"));
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Can you Turn West?"));
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Can you Move?"));
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Can you Drop Your Weapon?"));
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Can you Pick Up a Weapon?"));
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Can you Attack with a Weapon?"));

        try
        {
            Thread.sleep(20000); //20000 milliseconds 
        }
        catch (InterruptedException ex)
        {
            Thread.currentThread().interrupt();
        }
        c.currAmmo = 0;
        assertEquals(0, c.getCurrentAmmo());
        builder.inv.performReload();
        ;
        assertEquals(4, c.getCurrentAmmo());

    }
}