//RYAN HANDLEY
package ui.command;

import static org.junit.Assert.*;
import lifeform.Human;
import lifeform.LifeForm;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import weapon.ChainGun;
import weapon.Pistol;
import weapon.Weapon;
import environment.Environment;

public class TestDrop
{
    Environment env;

    LifeForm bob;

    Weapon weap1;

    Weapon weap2;

    /**
     * setup method for these tests
     */
    @Before
    public void setUp()
    {
        Environment.makeEnvironment(1, 1);
        env = Environment.getWorldInstance();
        bob = new Human("bob", 100, 50);
        env.addLifeFormToEnvironment(0, 0, bob);

        weap1 = new ChainGun();
        weap2 = new Pistol();

    }

    /**
     * tears down the environment after each test so that we don't 
     * start running into issues
     */
    @After
    public void TearDown()
    {
        env.destroyEnvironment();
    }

    /**
     * tests that a lifeForm can drop a weapon only if space is available
     */
    @Test
    public void dropWeaponWithSpace()
    {
        Weapon weap3 = new ChainGun();
        bob.addWeapon(weap3);
        assertEquals(weap3, bob.getWeapon());

        Drop drop = new Drop(bob);

        drop.performCommand();
        assertNull(bob.getWeapon());
    }

    /**
     * Tests to make sure that a lifeForm can't drop its weapon if it's cell is full
     */
    @Test
    public void testDropWeaponWithNoSpace()
    {
        Weapon weap3 = new ChainGun();
        bob.addWeapon(weap3);
        env.addWeaponToEnvironment(0, 0, weap1);
        env.addWeaponToEnvironment(0, 0, weap2);
        //the cell is now filled with weapons

        Drop drop = new Drop(bob);
        drop.performCommand();
        //lifeform should still have its weapon
        assertEquals(weap3, bob.getWeapon());

    }

}
