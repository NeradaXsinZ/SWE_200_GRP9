//RYAN HANDLEY
package ui.command;

import static org.junit.Assert.*;
import lifeform.Human;
import lifeform.LifeForm;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import weapon.ChainGun;
import weapon.Pistol;
import weapon.Weapon;
import environment.Environment;

public class TestAcquire
{

    Environment env;

    LifeForm bob;

    Weapon weap1;

    Weapon weap2;

    /**
     * setup method for these tests
     */
    @Before
    public void setUp()
    {
        Environment.makeEnvironment(1, 1);
        env = Environment.getWorldInstance();
        bob = new Human("bob", 100, 50);
        env.addLifeFormToEnvironment(0, 0, bob);

        weap1 = new ChainGun();
        weap2 = new Pistol();
        env.addWeaponToEnvironment(0, 0, weap1);
        env.addWeaponToEnvironment(0, 0, weap2);

    }

    /**
     * tears down the environment after each test so that we don't 
     * start running into issues
     */
    @After
    public void TearDown()
    {
        env.destroyEnvironment();
    }

    /**
     * Tests that a lifeForm can acquire a weapon 
     */
    @Test
    public void testAcquireWhenNoWeaponIsEquipped()
    {
        //makes sure the lifeForm doesnt have a weapon to start
        assertNull(bob.getWeapon());

        Acquire acq = new Acquire(bob);
        acq.findWeapon(2);
        acq.performCommand();
        assertEquals(weap2, bob.getWeapon());
    }

    /**
     * Tests that a lifeForm can't pick up a weapon when a weapon is already 
     * equipped
     */
    @Test
    public void testAcquireWhenWeaponIsEquipped()
    {
        //bob is entering the cell with a weapon
        Weapon weap3 = new Pistol();
        bob.addWeapon(weap3);

        //bob tries to pick up a new gun, but he can't
        //because he already has one
        Acquire acq = new Acquire(bob);
        acq.findWeapon(1);
        acq.performCommand();
        assertEquals(weap3, bob.getWeapon());
    }

}
