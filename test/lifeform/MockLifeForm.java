/*
 * Programmer:Reginald Nettey
 * Professor:Dr. Girard 
 * Course:Design Patterns
 * Description:Used to create a mock LifeForm to test LifeForm
 */
package lifeform;

public class MockLifeForm extends LifeForm
{
    /**
     * Creates an instance of MockLifeForm
     * @param name the name of the LifeForm
     * @param points the number of hit points the LifeForm has
     */
    public MockLifeForm(String name, int points)
    {
        super(name, points);
    }

    /**
     * Creates an instance of MockLifeForm
     * @param name the name of the LifeForm
     * @param points the number of hit points the LifeForm has
     * @param attackStr the attack strength of the lifeform
     */
    public MockLifeForm(String name, int points, int attackStr)
    {
        super(name, points, attackStr);
    }
}
