/*
 * Programmer:Reginald Nettey
 * Professor:Dr. Girard 
 * Course:Design Patterns
 * Description:Junit test for the alien class
 */
package lifeform;

import static org.junit.Assert.*;
import gameplay.SimpleTimer;
import gameplay.Timer;

import org.junit.Before;
import org.junit.Test;

import recovery.RecoveryLinear;

public class TestAlien
{
    Alien zerg;

    RecoveryLinear linear;

    Timer timer;

    @Before
    public void setup()
    {
        timer = new SimpleTimer();
        linear = new RecoveryLinear(10);
        zerg = new Alien("Zerg", 60, linear, 2);
    }

    /**
     * Tests that an aliens maxSpeed is set properly.
     */
    @Test
    public void testAliensMaxSpeed()
    {
        assertEquals(2, zerg.maxSpeed);
    }

    /**
     * Command Pattern tests start above this
     */
    @Test
    public void testAlienAttack()
    {
        assertEquals(10, zerg.getAttackStrength());
        assertEquals(60, zerg.getCurrentLifePoints());
        zerg.attack(zerg);
        assertEquals(50, zerg.getCurrentLifePoints());
    }

    @Test
    public void testRecoverRate()
    {
        assertEquals(2, zerg.getRecoveryRate());
    }

    /**Alien attacks itself with default alien attack(10)
     * and heals after 2 rounds
     */
    @Test
    public void testTimedRecovery()
    {
        zerg.setAttackStrength(20);
        timer.addTimeObserver(zerg);
        zerg.attack(zerg);
        timer.timeChanged();
        assertEquals(40, zerg.getCurrentLifePoints());
        timer.timeChanged();
        assertEquals(50, zerg.getCurrentLifePoints());
    }

    @Test
    public void testTimedRecoveryWithDifferentRate() throws InterruptedException
    {
        zerg.setRecoveryRate(3);
        timer.addTimeObserver(zerg);
        zerg.attack(zerg);
        timer.timeChanged();
        assertEquals(50, zerg.getCurrentLifePoints());
        timer.timeChanged();
        assertEquals(50, zerg.getCurrentLifePoints());
        timer.timeChanged();
        assertEquals(60, zerg.getCurrentLifePoints());
    }

    @Test
    public void testTimedRecoveryWithZeroRate()
    {
        zerg.setRecoveryRate(0);
        timer.addTimeObserver(zerg);
        zerg.attack(zerg);
        assertEquals(50, zerg.getCurrentLifePoints());
        timer.timeChanged();
        timer.timeChanged();
        timer.timeChanged();
        timer.timeChanged();
        timer.timeChanged();
        assertEquals(50, zerg.getCurrentLifePoints());
    }

    @Test
    public void testNoRecoveryWhenRemoveObserver()
    {
        zerg.setRecoveryRate(1);
        timer.addTimeObserver(zerg);
        zerg.attack(zerg);
        timer.timeChanged();
        assertEquals(60, zerg.getCurrentLifePoints());
        timer.removeTimeObserver(zerg);
        zerg.attack(zerg);
        timer.timeChanged();
        timer.timeChanged();
        assertEquals(50, zerg.getCurrentLifePoints());
    }

    /**Strategy Pattern(and pre-strategy) Tests*/
    @Test
    public void testAlienCreation()
    {
        assertEquals("Zerg", zerg.getName());
        assertEquals(60, zerg.getCurrentLifePoints());

    }

    @Test
    public void testSetLifePoints()
    {
        zerg.setCurrentLifePoints(50);
        assertEquals(50, zerg.getCurrentLifePoints());
        zerg.setCurrentLifePoints(-3);
        assertEquals(0, zerg.getCurrentLifePoints());
    }

    @Test
    public void testLinearRecoveryForAlien()
    {
        zerg.takeHit(20);
        zerg.recover();
        assertEquals(50, zerg.getCurrentLifePoints());
    }
}
