/*
 * Programmer:Reginald Nettey
 * Professor:Dr. Girard 
 * Course:Design Patterns
 * Description: Test the attributes of a Human
 */
package lifeform;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestHuman
{
    Human bob;

    @Before
    public void setup()
    {
        bob = new Human("Robert", 40, 10);
    }

    /**
     * Tests that human is initiated as 3 maxSpeed
     */
    @Test
    public void testMaxSpeedOfHuman()
    {
        assertEquals(3, bob.maxSpeed);
    }

    /**
     * Tests for Command patterns start above here
     */
    @Test
    public void testHumanAttackNoArmor()
    {
        bob.setArmorPoints(0);
        assertEquals(5, bob.getAttackStrength());
        assertEquals(40, bob.getCurrentLifePoints());
        bob.attack(bob);
        assertEquals(35, bob.getCurrentLifePoints());
    }

    @Test
    public void testArmoredHuman()
    {
        assertEquals(10, bob.getArmorPoints());
        assertEquals(5, bob.getAttackStrength());
        bob.attack(bob);
        assertEquals(40, bob.getCurrentLifePoints());
    }

    @Test
    public void testAttackHigherThanArmor()
    {
        bob.setArmorPoints(4);
        bob.attack(bob);
        assertEquals(39, bob.getCurrentLifePoints());
    }

    @Test
    public void testArmorEqualToDamage()
    {
        bob.setArmorPoints(5);
        bob.attack(bob);
        assertEquals(40, bob.getCurrentLifePoints());
    }

    /**Strategy(and pre_stategy) tests*/
    @Test
    public void testHumanCreation()
    {
        assertEquals("Robert", bob.getName());
        assertEquals(40, bob.getCurrentLifePoints());
        assertEquals(10, bob.getArmorPoints());
    }

    @Test
    public void testArmorPoints()
    {
        assertEquals(10, bob.getArmorPoints());
        bob.setArmorPoints(15);
        assertEquals(15, bob.getArmorPoints());
    }

    @Test
    public void testArmorPointsBelowZero()
    {
        assertEquals(10, bob.getArmorPoints());
        bob.setArmorPoints(-20);
        assertEquals(0, bob.getArmorPoints());
    }

}
