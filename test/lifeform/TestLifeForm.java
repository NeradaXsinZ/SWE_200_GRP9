/*
 * Programmer:Reginald Nettey
 * Professor:Dr. Girard 
 * Course:Design Patterns
 * Description:Junit tests for LifeForm Class
 */
package lifeform;

import static org.junit.Assert.*;
import environment.Environment;
import gameplay.SimpleTimer;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import weapon.Pistol;

public class TestLifeForm
{
    Environment world;

    LifeForm bob, jack;

    @Before
    public void setup()
    {
        Environment.makeEnvironment(4, 4);
        world = Environment.getWorldInstance();
        bob = new MockLifeForm("Robert", 40, 15);
        jack = new MockLifeForm("Jack", 40, 10);

    }

    @After
    public void complete()
    {
        world.destroyEnvironment();
    }

    /**
     * Tests that turning works properly on lifeform
     */
    @Test
    public void testTurning()
    {
        bob.turn("SOUTH");
        assertEquals("SOUTH", bob.currentDirection);

        bob.turn("EAST");
        assertEquals("EAST", bob.currentDirection);

        bob.turn("WEST");
        assertEquals("WEST", bob.currentDirection);

        bob.turn("NoRtH");
        assertEquals("NORTH", bob.currentDirection);
        //Prove that the method is case insensitive.
    }

    /**
     * Tests that a lifeform will be initialized with a direction of NORTH
     * and a max speed of 0.
     */
    @Test
    public void testDirectionAndMaxSpeedInitialization()
    {
        assertEquals(0, bob.maxSpeed);
        //Tests that a lifeforms starts out with a maxspeed of 0.
        assertEquals("NORTH", bob.currentDirection);
    }

    /**'
     * Command Pattern tests start above here
     */

    /**All these are modified to work environments range*/
    @Test
    public void testPistolOnLifeForm()
    {

        Pistol wep = new Pistol();
        world.addLifeFormToEnvironment(2, 0, bob);
        world.addLifeFormToEnvironment(0, 1, jack);
        bob.addWeapon(wep);
        assertEquals(22, world.computeRange(bob, jack));
        bob.attack(jack);
        assertEquals(33, jack.currentLifePoints);

    }

    /**
     * Tests from the Observer Lab start here
     */
    @Test
    public void testInitialization()
    {

        assertEquals("Robert", bob.getName());
        assertEquals(40, bob.getCurrentLifePoints());
    }

    @Test
    public void testAttack()
    {
        assertTrue(world.addLifeFormToEnvironment(0, 0, bob));
        assertTrue(world.addLifeFormToEnvironment(1, 0, jack));

        assertEquals(40, jack.getCurrentLifePoints());
        bob.attack(jack);
        assertEquals(25, jack.getCurrentLifePoints());
    }

    @Test
    public void testAttackingWhileDead()
    {
        world.addLifeFormToEnvironment(0, 0, bob);
        world.addLifeFormToEnvironment(1, 0, jack);
        assertEquals(40, jack.getCurrentLifePoints());
        bob.setAttackStrength(40);
        bob.attack(jack);
        assertEquals(0, jack.getCurrentLifePoints());
        jack.attack(bob);
        assertEquals(40, bob.getCurrentLifePoints());
    }

    @Test
    public void testTimePassing()
    {

        SimpleTimer timer = new SimpleTimer();
        timer.addTimeObserver(bob);
        assertEquals(0, bob.currentRound);
        timer.timeChanged();
        assertEquals(1, bob.currentRound);

    }

    /**Strategy Pattern(and pre-strategy) Tests*/

    @Test
    public void testLifePointsBelowZero()
    {
        LifeForm mike;
        mike = new MockLifeForm("Mike", -5);
        assertEquals(mike.getCurrentLifePoints(), 0);

    }

    @Test
    public void testTakeHit()
    {

        bob.takeHit(10);
        assertEquals(bob.getCurrentLifePoints(), 30);
        bob.takeHit(15);
        assertEquals(bob.getCurrentLifePoints(), 15);
        bob.takeHit(19);
        assertEquals(bob.getCurrentLifePoints(), 0);

    }

}
