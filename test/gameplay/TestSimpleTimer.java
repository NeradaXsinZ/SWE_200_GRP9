/*
 * Programmer:Reginald Nettey
 * Professor:Dr. Girard 
 * Course:Design Patterns
 * Description:Tests for SimpleTimer class
 */
package gameplay;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestSimpleTimer
{
    MockSimpleTimerObserver obsv;

    Timer timer;

    @Before
    public void setup()
    {
        obsv = new MockSimpleTimerObserver();
        timer = new SimpleTimer();
    }

    @Test
    public void testInitializationOfMockObject()
    {
        assertEquals(0, timer.getRound());
        assertTrue(timer.obsvIsEmpty());
    }

    @Test
    public void testAddObserver()
    {
        timer.addTimeObserver(obsv);
        assertFalse(timer.obsvIsEmpty());
        timer.timeChanged();
        assertEquals(1, obsv.myTime);

    }

    @Test
    public void testRemoveObserver()
    {
        timer.addTimeObserver(obsv);
        assertFalse(timer.obsvIsEmpty());
        timer.removeTimeObserver(obsv);
        assertTrue(timer.obsvIsEmpty());
    }

    @Test
    public void testTimeChangedWithObsv()
    {
        timer.addTimeObserver(obsv);
        timer.timeChanged();
        assertEquals(1, obsv.myTime);
    }

    @Test
    public void testTimeChangedWithoutObsv()
    {
        timer.timeChanged();
        assertEquals(1, timer.getRound());
    }

    @Test
    public void testSimpleTimerAsThread() throws InterruptedException
    {
        SimpleTimer st = new SimpleTimer(1000);
        st.start();
        Thread.sleep(250);
        for (int x = 0; x < 5; x++)
        {
            assertEquals(x, st.getRound());
            Thread.sleep(1000);
        }
    }
}

class MockSimpleTimerObserver implements TimeObserver
{
    public int myTime = 0;

    @Override
    public void updateTime(int time)
    {
        myTime = time;
    }
}
