/**@author Reginald Nettey*/
package gameplay;

import static org.junit.Assert.*;

import javax.swing.JOptionPane;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import environment.Environment;
import environment.Map;

public class TestSimulator
{
	Environment room;
	Simulator sim;
	SimpleTimer st;
	@Before 
	public void setup()
	{
		Environment.makeEnvironment(5, 10);
		st = new SimpleTimer();
		room = Environment.getWorldInstance();
		sim = new Simulator(5,6);


	}
	@After
	public void finish()
	{
		room.destroyEnvironment();
	}
	@Test		
	public void testInitialization()
	{
		Map m = new Map(room);
		m.updateMap(room);		
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is the map populated with humans, aliens, and weapons?"));
	}
	@Test
	public void testAddingLifeForms()
	{
		assertEquals(5, room.numHumans);
		assertEquals(6, room.numAliens);
		assertEquals(11, room.numWeapons);
	}
	@Test 
	public void testOnGoing() throws InterruptedException
	{
		SimpleTimer st = new SimpleTimer(1000);
		st.addTimeObserver(sim);
        st.start();
        Thread.sleep(250);
        for (int x = 0; x < 3; x++)
        {          
        	assertEquals(x,sim.round);
        	assertEquals(x,sim.ai.get(0).getRound());
            Thread.sleep(1000);

        }
	}

}
