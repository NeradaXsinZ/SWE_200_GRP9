package state;
import static org.junit.Assert.*;

import javax.swing.JOptionPane;

import lifeform.Human;

import org.junit.Test;

import state.AIContext;
import environment.Environment;
import environment.Map;
import weapon.Pistol;


public class TestAIContext
{
	/**
	 * Test each states intialization
	 */
	@Test
	public void testIntialization()
	{
		Human human = new Human("Bob", 50, 10);		//create human
		
		AIContext a = new AIContext(human);		//create AI context 
        assertTrue(a.getState() instanceof NoWeapon);	//make sure a's state is NoWeapon
        
        Pistol pist = new Pistol();		//create a pistol
        human.addWeapon(pist);		//human picks up weapon
        
        AIContext b = new AIContext(human);		//create AI context 
        assertTrue(b.getState() instanceof HasWeapon);	//make sure b's state is Weapon
        
        pist.setCurrentAmmo(0);
        AIContext c = new AIContext(human);		//create AI context 
        assertTrue(c.getState() instanceof OutOfAmmo);	//make sure c's state is OutOfAmmo
        
        human.takeHit(100);	//take 100 hit (killing human)
        AIContext d = new AIContext(human);		//create AI context 
        assertTrue(d.getState() instanceof Dead);	//make sure d's state is Dead
	}
	/**
	 * test that setState works properly
	 */
	@Test
	public void testChangeState()
	{
        Human human = new Human("Bob", 50, 10);		//create human
        
        AIContext bob = new AIContext(human);		//create AI context 
        assertTrue(bob.getState() instanceof NoWeapon);	//make sure bobs state is NoWeapon
        bob.setState(new Dead(human, bob));		//set state to dead 
        assertTrue(bob.getState() instanceof Dead);		//test that bobs state is 

	}
	
	/**
	 * test that it evaluates the current state
	 */
	@Test
	public void testActiveStateEvaluation()
	{
		Environment.makeEnvironment(5, 5);		//make environment
		Environment environment = Environment.getWorldInstance();		//get environment
        Human human = new Human("Bob", 50, 10);	//create human
        Pistol pist = new Pistol();		//create a pistol
        human.addWeapon(pist);		//human picks up weapon
        human.takeHit(100);	//take 100 hit (killing human)
        
        environment.addLifeFormToEnvironment(3, 3, human);		//add human to 3,3
        AIContext bob = new AIContext(human);		//createAIContext
        
        bob.setState(new Dead(human, bob));		//set to 'Dead'
        assertTrue(bob.getState() instanceof Dead);		//test the ActionState is dead 
        Map map = new Map(environment);
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there " + "a human\n being displayed at 3,3"));	//test human in 3,3

        bob.execute();	//execute bob (in Dead state)
        map.updateMap(environment);	//update the map
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there " + "a human and gun\n being displayed somewhere"));	//there should be a human and gun 

	}
	
	

}
