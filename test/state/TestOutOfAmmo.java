package state;
import static org.junit.Assert.*;



import lifeform.Alien;
import lifeform.Human;

import org.junit.Test;

import state.AIContext;
import weapon.Pistol;
import environment.Environment;



public class TestOutOfAmmo
{

	/**
	 * test that on create of an AI context the OutOfAmmo state initializes correctly
	 */
	@Test
	public void testIntialization()
	{
		Human human1 = new Human("Bob", 50, 0);		//human1	
		
		Pistol p = new Pistol();	//create a pistol 
		
		human1.addWeapon(p); 		//add a pistol to human1 
		p.setCurrentAmmo(0);	//set Ammo to 0 to test the initialization
        
        AIContext bob = new AIContext(human1);		///create ai for human called bob
        
        assertTrue(bob.getState()instanceof OutOfAmmo);		//bobs state should be outofammo
       
        ActionState ooa =  bob.getState();		//ooa1 (OutOfAmmo) bob's state
        assertTrue(ooa instanceof OutOfAmmo);
        assertEquals(0, human1.getWeapon().getCurrentAmmo());	//test the gun has no ammo
		
	}
	
	/**
	 * test the state reloads when it isn't dead
	 */
	@Test
	public void testReload()
	{
		Human human1 = new Human("Bob", 50, 0);		//human1	
		
		Pistol p = new Pistol();	//create a pistol
		
		human1.addWeapon(p); 		//add a pistol to human1 
		p.setCurrentAmmo(0);	//set ammo to 0 for the pistol p
		        
        AIContext bob = new AIContext(human1);		///create ai for human called bob
        
        assertTrue(bob.getState()instanceof OutOfAmmo);		//bobs state should be outofammo
       
        ActionState ooa =  bob.getState();		//ooa1 (OutOfAmmo) bob's state
        assertTrue(ooa instanceof OutOfAmmo);		//test the state is OutOfAmmo
        assertEquals(0, human1.getWeapon().getCurrentAmmo());	//test the gun actually has no ammo
        
        ooa.evaluation();		//evaluate
        
        assertEquals(10, human1.getWeapon().getCurrentAmmo());	//test the gun was reload
		assertTrue(ooa instanceof HasWeapon);
	}
	
	/**
	 * test for if the lifeform dies
	 * that when OutOfAmmo evaluates it changes it to Dead and doesn't reload
	 */
	@Test
	public void testDead()
	{
		Human human1 = new Human("Bob", 50, 0);		//human1	
		Pistol p = new Pistol();	//create a pistol
		
		human1.addWeapon(p); 		//add a pistol to human1 
		p.setCurrentAmmo(0);	//set ammo to 0 so when AI is created its OutOfAmmo
		        
        AIContext bob = new AIContext(human1);		///create AI for human called bob
        
        assertTrue(bob.getState()instanceof OutOfAmmo);		//bobs state should be OutOfAmmo
       
        ActionState ooa =  bob.getState();		//ooa1 (OutOfAmmo) bob's state
        assertTrue(ooa instanceof OutOfAmmo);	//test that the state is OutOfAmmo
        assertEquals(0, human1.getWeapon().getCurrentAmmo());	//test the gun actually has no ammo
        human1.takeHit(100);
        
        ooa.evaluation();		//evaluate
        
        assertEquals(0, human1.getWeapon().getCurrentAmmo());	//test the gun was reload
		assertTrue(bob.getState() instanceof Dead);	//test that bobs state was changed to dead
	
	}


}
