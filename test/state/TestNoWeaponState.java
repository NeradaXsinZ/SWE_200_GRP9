//Ryan Handley
package state;

import static org.junit.Assert.*;
import lifeform.Human;
import lifeform.LifeForm;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import weapon.ChainGun;
import weapon.Pistol;
import weapon.Weapon;
import environment.Environment;
import environment.Map;

public class TestNoWeaponState
{
	Environment env;
	LifeForm lf;
	Map map;
	
	/**
	 * setup method
	 */
	@Before
	public void setUp()
	{
		Environment.makeEnvironment(5,5);
		env = Environment.getWorldInstance();
		map = new Map(env);
	}
	
	/**
	 * TearDown method
	 */
	@After
	public void tearDown()
	{
		env.destroyEnvironment();
	}
	
	/**
	 * tests that the lifeform will pick up the weapon in its cell if one is present
	 */
	@Test
	public void testWhenWeaponIsInCell()
	{
		ChainGun ch = new ChainGun();
		Human bob = new Human("Bob",100,50);
		assertNull(bob.getWeapon());
		
		env.addWeaponToEnvironment(1, 1, ch);		//weapon and lifeform are in the same cell
		env.addLifeFormToEnvironment(1, 1, bob);
		map.updateMap(env);
		
		AIContext ai = new AIContext(bob);
		NoWeapon nw = new NoWeapon(bob,ai);
		nw.evaluation();
		map.updateMap(env);
		
		assertEquals(bob.getWeapon(),ch);//bob should have the chaingun
		assertTrue(ai.getState() instanceof HasWeapon);	//the lifeform has picked up a weapon, and it should 
														//should now be in the hasweapon state
	}
	
	/**
	 * tests that the lifeForm will not pick up a weapon if there is not one in it's 
	 * current cell
	 */
	@Test
	public void testWhenNoWeaponIsInCell()
	{
		ChainGun ch = new ChainGun();
		Human bob = new Human("Bob",100,50);
		
		env.addWeaponToEnvironment(1,1,ch);
		env.addLifeFormToEnvironment(0,0,bob);
		map.updateMap(env);
		
		AIContext ai = new AIContext(bob);
		NoWeapon nw = new NoWeapon(bob,ai);
		nw.evaluation();
		map.updateMap(env);
		
		assertNull(bob.getWeapon());			//bob should have no weapon
		assertTrue(ai.getState() instanceof NoWeapon);
	}
	
	/**
	 * tests that the lifeForm no longer carries out noweapon functionality 
	 * when it is dead
	 */
	@Test
	public void testWhenLifeFormIsDead()
	{
		Human bob = new Human("Bob",100,0);
		ChainGun ch = new ChainGun();
		
		env.addWeaponToEnvironment(3,3,ch);
		env.addLifeFormToEnvironment(1,1,bob);
		map.updateMap(env);
		
		AIContext ai = new AIContext(bob);
		NoWeapon nw = new NoWeapon(bob,ai);
		nw.evaluation();
		map.updateMap(env);
		
		int xpos = bob.getXLoc();
		int ypos = bob.getYLoc();	
		bob.takeHit(100);			//the lifeform should now be dead, and should switch to the dead state
		nw.evaluation();
		assertTrue(ai.getState()instanceof Dead);
		
		
	}

}
