package state;

import static org.junit.Assert.*;

import javax.swing.JOptionPane;

import lifeform.Human;

import org.junit.After;
import org.junit.Before;
import org.junit.Test;

import state.AIContext;
import state.Dead;

import weapon.Pistol;

import environment.Environment;
import environment.Map;

/**
 * Test for the dead state
 * @author David Burkett
 *
 */
public class TestDeadState
{

    Environment world;

    Map map;

    /**
     * Setup the everything for us
     */
    @Before
    public void setup()
    {
        Environment.makeEnvironment(7, 7);
        world = Environment.getWorldInstance();
        map = new Map(world);
    }

    @After
    public void destory()
    {
        world.destroyEnvironment();
    }

    /**
     * Test that a lifeform will respawn and drop its weapon in a random location and the lifeform
     * will respawn in a random location.
     */
    @Test
    public void test()
    {
        Human fred = new Human("Fred", 50, 50);
        Pistol pist = new Pistol();
        fred.addWeapon(pist);
        world.addLifeFormToEnvironment(6, 6, fred);
        map.updateMap(world);
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there " + "a human\n being displayed in cell (6,6)"));

        AIContext ai = new AIContext(fred);
        Dead ds = new Dead(fred, ai);
        fred.takeHit(100);
        ds.evaluation();
        map.updateMap(world);
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there " + "a human\n somewhere and a weapons somewhere"));
    }
}
