/**
 * @TODO Tim Haus
 */
package state;

import static org.junit.Assert.*;

import javax.swing.JOptionPane;

import lifeform.Alien;
import lifeform.Human;

import org.junit.Test;

import weapon.Pistol;
import environment.Environment;
import environment.Map;

public class TestHasWeaponState
{

    //Tests for the target of a different type
    @Test
    public void testForTargetOfDifferentType()
    {
        Environment world;
        Map map;

        Environment.makeEnvironment(7, 7); //Makes an environment
        world = Environment.getWorldInstance(); //Makes a world of the instance of environment
        map = new Map(world); //makes a new map GUI

        Human fred = new Human("Fred", 50, 50); //makes a human
        Pistol pist = new Pistol(); //makes a pistol
        fred.addWeapon(pist); //adds weapons to human
        world.addLifeFormToEnvironment(6, 6, fred); //adds the human to the environment
        map.updateMap(world); //updates the map
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there " + "a human\n being displayed in cell (6,6)")); // Tests if there is a human at 6,6

        Alien tim = new Alien("tim", 10, null, 0); //Creates an alien
        Pistol gun = new Pistol(); //makes a new pistol
        tim.addWeapon(gun); //adds the pistol to the alien
        world.addLifeFormToEnvironment(6, 5, tim); //adds the alien to 6,5
        map.updateMap(world); //updates the map
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there " + "a Alien\n being displayed in cell (6,5)")); //test if the map adds an alien to 6,5

        AIContext ai = new AIContext(fred); //new AIContext with the lifeform in it
        HasWeapon li = new HasWeapon(fred, ai); //puts it in the hasweapon state
        li.evaluation(); //starts the method

        assertEquals(0, tim.getCurrentLifePoints()); //tests that the alien dies
        map.updateMap(world); //updates the map
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there " + "no Alien\n being displayed in cell (6,5)")); // tests if the map displays no alien

        world.destroyEnvironment(); //destroys the world
    }

    //Tests if there is no other targets
    @Test
    public void testForNoTarget()
    {
        Environment world; //LOOK AT FIRST TESTS COMMENTS THEY ARE VERY SIMILAR
        Map map;

        Environment.makeEnvironment(7, 7);
        world = Environment.getWorldInstance();
        map = new Map(world);

        Human fred = new Human("Fred", 50, 50);
        Pistol pist = new Pistol();
        fred.addWeapon(pist);
        world.addLifeFormToEnvironment(6, 6, fred);
        map.updateMap(world);
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there " + "a human\n being displayed in cell (6,6)"));

        AIContext ai = new AIContext(fred);
        HasWeapon li = new HasWeapon(fred, ai);
        li.evaluation();
        map.updateMap(world);

        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there " + "a human\n that moved or turned randomly?"));
        world.destroyEnvironment();
    }

    //tests if the same type lifeform will take any hits
    @Test
    public void testForTargetOfSameType()
    {
        Environment world; //LOOK AT COMMENTS ON THE FIRST TEST
        Map map;

        Environment.makeEnvironment(7, 7);
        world = Environment.getWorldInstance();
        map = new Map(world);

        Human fred = new Human("Fred", 50, 50);
        Pistol pist = new Pistol();
        fred.addWeapon(pist);
        world.addLifeFormToEnvironment(6, 6, fred);
        map.updateMap(world);
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there " + "a human\n being displayed in cell (6,6)"));

        Human tim = new Human("tim", 50, 50);
        Pistol gun = new Pistol();
        fred.addWeapon(gun);
        world.addLifeFormToEnvironment(6, 5, tim);
        map.updateMap(world);
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there " + "a human\n being displayed in cell (6,5)"));

        AIContext ai = new AIContext(fred);
        HasWeapon li = new HasWeapon(fred, ai);

        li.evaluation();

        assertEquals(50, tim.getCurrentLifePoints());
        map.updateMap(world);

        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there " + "a human\n that moved or turned randomly, \nbut depending on the random number \n it could have not moved and Is \nthe other human still alive?"));
        world.destroyEnvironment();
    }

    //Tests with one shot left
    @Test
    public void testForTargetWithOneShotLeft()
    {
        Environment world; //LOOK AT COMMENTS ON THE FIRST TEST
        Map map;

        Environment.makeEnvironment(7, 7);
        world = Environment.getWorldInstance();
        map = new Map(world);

        Human fred = new Human("Fred", 50, 50);
        Pistol pist = new Pistol();
        fred.addWeapon(pist);
        world.addLifeFormToEnvironment(6, 6, fred);
        map.updateMap(world);
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there " + "a human\n being displayed in cell (6,6)"));

        Alien tim = new Alien("tim", 50, null, 0);
        Pistol gun = new Pistol();
        tim.addWeapon(gun);
        world.addLifeFormToEnvironment(6, 5, tim);
        map.updateMap(world);
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there " + "a Alien\n being displayed in cell (6,5)"));

        pist.setCurrentAmmo(1);                              //sets the current ammo to one

        AIContext ai = new AIContext(fred);
        HasWeapon li = new HasWeapon(fred, ai);

        
        
        li.evaluation();

        assertEquals(40, tim.getCurrentLifePoints());

        
        assertEquals(10, pist.getCurrentAmmo());

        //TODO ATTENTION: THIS SHOULD TEST CORRECTLY BUT ONE MEMBER OF THE GROUP HAS NOT DONE THE OUT OF AMMO STATE
        assertEquals(10, gun.getCurrentAmmo());

        map.updateMap(world);

        world.destroyEnvironment();
    }

    //Tests if the target is dead what it will do
    @Test
    public void testForTargetThatIsDead()
    {
        Environment world; //LOOK AT FIRST TEST
        Map map;

        Environment.makeEnvironment(7, 7);
        world = Environment.getWorldInstance();
        map = new Map(world);

        Human fred = new Human("Fred", 50, 50);
        Pistol pist = new Pistol();
        fred.addWeapon(pist);
        world.addLifeFormToEnvironment(6, 6, fred);
        map.updateMap(world);
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Is there " + "a human\n being displayed in cell (6,6)"));

        fred.takeHit(100);                          //Kills fred to make him have no lifepoints
        map.updateMap(world);
        assertEquals(JOptionPane.YES_OPTION, JOptionPane.showConfirmDialog(null, "Did the " + " human\n being displayed in cell (6,6) stay alive?"));
        assertEquals(0, fred.getCurrentLifePoints());

        AIContext ai = new AIContext(fred);
        HasWeapon li = new HasWeapon(fred, ai);
        li.evaluation();

        map.updateMap(world);
        world.destroyEnvironment();
    }

}
