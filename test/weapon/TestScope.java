package weapon;

import weapon.Pistol;
import weapon.Scope;

import static org.junit.Assert.*;
import org.junit.Test;

/**
 * Holds the test cases for the Scope class.
 * @author Jeff Titanich
 */
public class TestScope
{
    /**
     * Tests that a pistol still hits and deals the correct
     * damage at range greater than its maxRange.
     */
    @Test
    public void testFunctionality()
    {
        Pistol wep = new Pistol();
        Scope scope = new Scope(wep);
        wep.setRange(60);
        assertEquals(0, scope.calculateDamage());
        wep.setRange(55);
        assertEquals(0, scope.calculateDamage());
        wep.setRange(10);
        wep.newRound();
        assertEquals(12, scope.calculateDamage());
    }
}
