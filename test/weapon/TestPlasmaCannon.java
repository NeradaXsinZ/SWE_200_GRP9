package weapon;

import static org.junit.Assert.*;
import org.junit.Test;

import weapon.PlasmaCannon;

/**
 * Holds the test cases for the PlasmaCannon class.
 * @author Jeff Titanich
 */
public class TestPlasmaCannon
{
    /**
     * Tests that all the base values are correct at instantiation.
     */
    @Test
    public void testBaseStats()
    {
        PlasmaCannon plasma = new PlasmaCannon();
        assertEquals(50, plasma.getBaseDamage());
        assertEquals(4, plasma.getMaxAmmo());
        assertEquals(40, plasma.getMaxRange());
        assertEquals(1, plasma.getRateOfFire());
    }

    /**
     * Tests that a plasma cannon will deal the correct damage based
     * on range to the target.
     */
    @Test
    public void testRanges()
    {
        PlasmaCannon plasma = new PlasmaCannon();
        plasma.setRange(41);
        assertEquals(0, plasma.calculateDamage());
        plasma.newRound();
        plasma.reload();
        plasma.setRange(10);
        assertEquals(50, plasma.calculateDamage());
    }

    /**
     * Tests that a plasma cannon will deal the correct damage based on
     * different ammo values.
     */
    @Test
    public void testAmmos()
    {
        PlasmaCannon plasma = new PlasmaCannon();
        plasma.setRange(41);
        assertEquals(0, plasma.calculateDamage());
        plasma.newRound();
        plasma.setRange(10);
        assertEquals(37, plasma.calculateDamage());
    }
}
