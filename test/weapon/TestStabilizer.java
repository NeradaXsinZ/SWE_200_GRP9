package weapon;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * Tests for the stabalizer attatchment.
 * @author David Burkett 
 *
 */
public class TestStabilizer
{
    /**
     * Tests that Stabilizer works with the Pistol
     */
    @Test
    public void testStabilizerAndPistol()
    {
        Pistol pistol = new Pistol();
        Stabilizer stab = new Stabilizer(pistol);
        pistol.setRange(0);
        assertEquals(15, stab.calculateDamage());
        //Make sure it increases damage by 25%

        for (int i = 0; i < 15; i++)
        {
            pistol.newRound();
            assertEquals(15, stab.calculateDamage());
        }
        //Make sure it auto reloads   

    }

    /**
     * Tests that no more than 2 attachments can be used
     */
    @Test
    public void testThirdAttachment()
    {
        Pistol pistol = new Pistol();
        Stabilizer stab1 = new Stabilizer(pistol);
        Stabilizer stab2 = new Stabilizer(stab1);
        pistol.setRange(0);
        assertEquals(18, stab2.calculateDamage());

    }

    /**
     * Tests with other attachment
     */
    @Test
    public void testWithScope()
    {
        ChainGun cg = new ChainGun();
        Stabilizer st = new Stabilizer(cg);
        Scope sc = new Scope(st);
        st.setRange(30);
        ;

        assertEquals(12, sc.calculateDamage());

    }

}
