package weapon;

import static org.junit.Assert.*;

import org.junit.Test;

/**
 * The test cases for pistol
 * 
 * @author David Burkett (Disco-Dave)
 */
public class TestPistol
{
    /**
     * Tests for pistol initialization
     */
    @Test
    public void testPistolInitialization()
    {
        Pistol pistol = new Pistol();

        assertEquals(50, pistol.maxRange);
        //The current range should be equivalent the max range

        assertEquals(10, pistol.baseDamage);
        //Tests the pistol baseDamage is 10

        assertEquals(2, pistol.rateOfFire);
        //Tests the rate of fire

        assertEquals(10, pistol.maxAmmo);
        //Tests the max ammo is 10
    }

    /**
     * Test the pistol functions properly
     */
    @Test
    public void testFiringPistol()
    {
        Pistol pistol = new Pistol();
        pistol.setRange(51);
        assertEquals(0, pistol.calculateDamage());
        //Make sure the pistol can't cause damage past
        //the max range.
        assertEquals(9, pistol.currAmmo);
        //Make sure a shot is fired if out of function

        pistol.newRound();
        pistol.setRange(10);
        assertEquals(10, pistol.calculateDamage());
        //When range is 10, then the damage should be 10

        pistol.setRange(30);
        assertEquals(6, pistol.calculateDamage());
        //Ensure we can do some damage

        pistol.newRound();
        pistol.setRange(-5);
        //If set range is negative we want it to set 0 instead
        //of the negative number

        assertEquals(12, pistol.calculateDamage());
        //Ensure we can do more damage

        for (int i = 0; i < 10; i++)
        {
            pistol.newRound();
            pistol.calculateDamage();
        }
        assertEquals(0, pistol.calculateDamage());
        //Ensures the pistol will not fire after it runs
        //out of ammmo
        pistol.newRound();
        pistol.reload();
        assertEquals(12, pistol.calculateDamage());
        //Ensure the reload functions works

        pistol.newRound();
        for (int i = 0; i < 2; i++)
        {
            pistol.calculateDamage();
        }
        assertEquals(0, pistol.calculateDamage());
        //Ensure the gun will not fire more than twice
        //a round.
    }
}
