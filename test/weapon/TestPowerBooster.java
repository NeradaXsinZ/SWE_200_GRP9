/**
 * @author Reginald Nettey
 * Junit tests for powerBooster
 */
package weapon;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestPowerBooster
{

    GenericWeapon pistol;

    PowerBooster pB;

    ChainGun chain;

    @Before
    public void setup()
    {
        pistol = new Pistol();
        chain = new ChainGun();
        pB = new PowerBooster(pistol);

    }

    @Test
    public void testInitialization()
    {
        pistol.setRange(0);
        assertEquals(24, pB.calculateDamage());

    }

    @Test
    public void testPowerBoosterDamage()
    {
        pistol.setRange(30);
        assertEquals(12, pB.calculateDamage());
        pistol.setRange(10);
        assertEquals(19, pB.calculateDamage());

    }

    @Test
    public void testPowerBoosterNoAmmo()
    {
        GenericWeapon pistol = new Pistol();
        PowerBooster pB = new PowerBooster(pistol);
        pistol.setRange(50);
        for (int x = 0; x < 10; x++)
        {
            if (x % 2 == 0 && x != 0)
            {
                pistol.newRound();
            }
            pB.calculateDamage();

        }
        pistol.newRound();
        assertEquals(0, pB.calculateDamage());
    }

    @Test
    public void testPBWrapsStabilizer()
    {
        Pistol p = new Pistol();
        Stabilizer sta = new Stabilizer(p);
        p.setRange(10);
        PowerBooster pA = new PowerBooster(sta);
        assertEquals(0, p.getWrapped());
        assertEquals(10, sta.getMaxAmmo());
        assertEquals(12, sta.calculateDamage());
        assertEquals(22, pA.calculateDamage());

    }

    @Test
    public void testPBWrapsScope()
    {
        Scope sco = new Scope(chain);
        chain.setRange(71);
        assertEquals(0, sco.calculateDamage());
        chain.setRange(70);
        /**
         * If you did the math it would be 37.9 but since
         * chainGun and scope both round down it is 1 less
         * than it is supposed to be and i assumed that would be okay
         * to work off of those values
         * This is so for a few of the tests
         */
        assertEquals(36, sco.calculateDamage());
        PowerBooster pA = new PowerBooster(sco);
        chain.reload();
        ;
        assertEquals(72, pA.calculateDamage());

    }

    @Test(expected = NullPointerException.class)
    public void testMoreThanTwoAttachments()
    {
        Pistol p = new Pistol();
        Stabilizer sta = new Stabilizer(p);
        PowerBooster boost = new PowerBooster(sta);
        Scope scope = new Scope(boost);
        assertEquals(1, scope.getWrapped());

        //This will throw an exception as it was never added
        assertEquals(1, scope.calculateDamage());

    }

}
