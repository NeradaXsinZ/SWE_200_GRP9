/**
 * @author Reginald Nettey
 */
package weapon;

import static org.junit.Assert.*;

import org.junit.Before;
import org.junit.Test;

public class TestChainGun
{
    ChainGun chainGun;

    @Before
    public void setup()
    {
        chainGun = new ChainGun();
    }

    @Test
    public void testMaxAmmo()
    {
        assertEquals(40, chainGun.maxAmmo);
        //chainGun.calculateDamage();
    }

    @Test
    public void testInitialization()
    {
        chainGun.setRange(40);
        assertEquals(40, chainGun.getRange());
        assertEquals(60, chainGun.maxRange);
        assertEquals(15, chainGun.baseDamage);
    }

    @Test
    public void testChainGunMaxRange()
    {
        chainGun.setRange(60);

        assertEquals(15, chainGun.calculateDamage());
    }

    @Test
    public void testChainGunLowRange()
    {
        chainGun.setRange(20);
        assertEquals(5, chainGun.calculateDamage());
    }

    @Test
    public void testChainGunHigherThanMaxRange()
    {
        chainGun.setRange(70);
        assertEquals(17, chainGun.calculateDamage());
    }

    @Test
    public void testReload()
    {
        assertEquals(40, chainGun.getCurrentAmmo());
        for (int i = 0; i < 44; i++)
        {
            chainGun.calculateDamage();
        }
        assertEquals(0, chainGun.getCurrentAmmo());
        chainGun.reload();
        assertEquals(40, chainGun.getCurrentAmmo());
    }

}
